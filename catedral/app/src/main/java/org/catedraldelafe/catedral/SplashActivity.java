package org.catedraldelafe.catedral;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.catedraldelafe.catedral.util.Configuration;

public class SplashActivity extends AppCompatActivity {
    protected boolean activo = true;
    protected int tiempoSplash = 600;
    private Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        activity.setTitle("");

        //crea y ejecuta un hilo
        Thread threadSplash = new Thread(){
            public void run(){
                try{
                    int timer = 0;
                    while(activo && timer < tiempoSplash){
                        sleep( Integer.parseInt(Configuration.getProperty("tiempo.espera", getApplicationContext())) *  100);
                        if(activo){
                            timer += 100;
                        }
                    }
                }catch(InterruptedException ie){
                    ie.printStackTrace();
                }finally{
                    abrirAplicacion();
                    //stop();
                }
            }
        };
        threadSplash.start();


    }


    public void abrirAplicacion(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }
}
