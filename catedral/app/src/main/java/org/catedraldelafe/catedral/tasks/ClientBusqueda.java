package org.catedraldelafe.catedral.tasks;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.catedraldelafe.catedral.ResultadoBusquedaActivity;
import org.catedraldelafe.catedral.dominio.Data;
import org.catedraldelafe.catedral.dominio.ValueDataEvento;
import org.catedraldelafe.catedral.dominio.ValueDataPublicacion;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by caroapaza on 20/01/2016.
 */
public class ClientBusqueda extends AsyncTask<String,Void,String> {
    private static final String TAG = "ClientBusqueda";
    public JSONObject valueData = null;
    private Activity activity;
    private String palabra;
    private String title;
    private int page;
    private AlertDialog alertDialog;
    private ProgressDialog dialog;

    public ClientBusqueda(Activity activity, String palabra, int page,String title){
        this.activity = activity;
        this.palabra = palabra;
        this.page =  page;
        this.title=title;
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(this.activity);
        dialog.setMessage("Espere ...");
        dialog.setIndeterminate(true);
        dialog.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... urls) {
        String resultado = null;
        try{
            resultado = readJSONFeed(urls[0]);//https://catedraldelafe.herokuapp.com/api/search
        }catch (JSONException jsone){
            jsone.printStackTrace();
        }
        return resultado;
    }

    @Override
    protected void onPostExecute(String result) {
        Gson gson = new Gson();
        List<Object> listaData = new ArrayList<Object>();

        //result="{\\r\\n  \\\"status\\\": \\\"ok\\\",\\r\\n  \\\"data\\\": {\\r\\n    \\\"total\": 6,\\r\\n    \\\"per_page\\\": 5,\\r\\n    \\\"current_page\\\": 1,\\r\\n    \\\"last_page\\\": 2,\\r\\n    \\\"next_page_url\\\": \\\"http:\\/\\/192.168.0.157:90\\/api\\/posts?page=2\\\",\\r\\n    \\\"prev_page_url\\\": null,\\r\\n    \\\"from\\\": 1,\\r\\n    \\\"to\\\": 5,\\r\\n    \\\"data\\\": [\\r\\n      {\\r\\n        \\\"id\\\": \\\"110\\\",\\r\\n        \\\"title\\\": \\\"C\\u00F3mo descubrir la voluntad de Dios\\\",\\r\\n        \\\"description\\\": \\\"<p class=\\\\\\\"Contenido\\\\\\\" style=\\\\\\\"font-size: 12px; font-family: Arial, Helvetica, sans-serif;\\\\\\\" align=\\\\\\\"justify\\\\\\\">&iquest;C&oacute;mo&nbsp; tomar determinaciones&nbsp; seguros que estamos en la voluntad de Dios?\\\\\\\" &iquest;Debo casarme con esta chica? Yo creo que realmente Dios nos ha tra&iacute;do el uno al otro, pero quisiera saber qu&eacute; tiene Dios planeado para nuestro futuro\\\\\\\".<\\/p>\\\\r\\\\n<p style=\\\\\\\"font-family: \\'Times New Roman\\'; font-size: medium;\\\\\\\" align=\\\\\\\"justify\\\\\\\"><span class=\\\\\\\"Contenido\\\\\\\" style=\\\\\\\"font-size: 12px; font-family: Arial, Helvetica, sans-serif;\\\\\\\">\\\\\\\"Me ofrecieron un empleo en otra regi&oacute;n del pa&iacute;s. Tiene mucho en su favor. &iquest;Podr&iacute;a ayudarme Dios a tomar una decisi&oacute;n acertada?\\\\\\\"<br \\/><br \\/>\\\\\\\"Mi hija est&aacute; en el &uacute;ltimo a&ntilde;o de escuela superior y tiene la oportunidad de seguir estudiando en varias universidades. &iquest;C&oacute;mo puedo saber cu&aacute;l es la que m&aacute;s le conviene?&nbsp;<br \\/><br \\/>Aunque las Escrituras no nos dan una f&oacute;rmula precisa para descubrir la voluntad de Dios en estas circunstancias intensamente personales, s&iacute; nos dicen claramente que tenemos un Gu&iacute;a, el Se&ntilde;or Jesucristo, que nos dirigir&aacute; en la direcci&oacute;n acertada y nos ayudar&aacute; a elegir lo que es fundamentalmente correcto tanto en las decisiones mayores como en las menores.<br \\/><br \\/>Dios demanda que busquemos racionalmente su voluntad para nuestras vidas (nuestra parte), pero al final de cuentas debemos depender de El para obtener la orientaci&oacute;n segura y certera (su parte). Nosotros no tenemos todos los detalles, pero s&iacute; tenemos un Gu&iacute;a que todo lo sabe, que nos ama en todo momento y que todo lo puede, Jesucristo, que promete su direcci&oacute;n a sus seguidores (su parte).<br \\/><\\/span><\\/p>\\\",\\r\\n        \\\"date\\\": \\\"1456228552\\\",\\r\\n        \\\"counter\\\": \\\"0\\\",\\r\\n        \\\"type_id\\\": \\\"2\\\",\\r\\n        \\\"user_id\\\": \\\"1\\\",\\r\\n        \\\"created_at\\\": \\\"2016-02-23 11:55:52\\\",\\r\\n        \\\"updated_at\\\": \\\"2016-02-23 11:55:52\\\",\\r\\n        \\\"images\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"134\\\",\\r\\n            \\\"url\\\": \\\"images\\/posts\\/catedral27.jpg\\\",\\r\\n            \\\"post_id\\\": \\\"110\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:55:52\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:55:52\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"videos\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"70\\\",\\r\\n            \\\"url\\\": \\\"https:\\/\\/www.youtube.com\\/v\\/eR79OdTeaYU\\\",\\r\\n            \\\"post_id\\\": \\\"110\\\",\\r\\n            \\\"youtube_id\\\": \\\"eR79OdTeaYU\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:55:52\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:55:52\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"user\\\": {\\r\\n          \\\"id\\\": \\\"1\\\",\\r\\n          \\\"name\\\": \\\"gonzalo huanco\\\",\\r\\n          \\\"email\\\": \\\"gonzalo@gmail.com\\\",\\r\\n          \\\"role_id\\\": \\\"1\\\",\\r\\n          \\\"created_at\\\": \\\"2015-12-10 11:24:17\\\",\\r\\n          \\\"updated_at\\\": \\\"2015-12-10 11:24:17\\\"\\r\\n        }\\r\\n      },\\r\\n      {\\r\\n        \\\"id\\\": \\\"109\\\",\\r\\n        \\\"title\\\": \\\"Cristo y el Creyente\\\",\\r\\n        \\\"description\\\": \\\"<p class=\\\\\\\"Contenido\\\\\\\" style=\\\\\\\"font-size: 12px; font-family: Arial, Helvetica, sans-serif;\\\\\\\" align=\\\\\\\"justify\\\\\\\">Como el lirio entre las espinas, as! es mi amiga entre las doncellas. Como el manzano entre los &aacute;rboles silvestres, as! es mi amado entre los mancebos: bajo la sombra del deseado me sent&eacute; con gran deleite y su fruto fue dulce a mi paladar&raquo; (Cantar de los Cantares 2:2 3).<\\/p>\\\\r\\\\n<p class=\\\\\\\"Contenido\\\\\\\" style=\\\\\\\"font-size: 12px; font-family: Arial, Helvetica, sans-serif;\\\\\\\" align=\\\\\\\"justify\\\\\\\">Si una persona no convertida fuese llevada al cielo, donde est&aacute; Cristo sentado en gloria, y oyese las palabras de amor que Cristo, lleno de admiraci&oacute;n, dirige al creyente, y podr&iacute;a entenderlas; no podr&iacute;a comprender c&oacute;mo Cristo puede descubrir belleza alguna en la despreciable gente religiosa a quien &eacute;l, en el fondo de su coraz&oacute;n, menosprecia.&nbsp;<br \\/><br \\/>Y si un inconverso pudiese o&iacute;r a un cristiano en sus devociones cuando realmente ha transpuesto el velo y se enterase de sus palabras de admiraci&oacute;n y amor hacia Cristo, tampoco podr&iacute;a en modo alguno comprenderlas; no le ser&iacute;a posible entender c&oacute;mo el creyente puede tener tan encendido amor hacia un ser que no ha visto, en quien &eacute;l mismo no ve atractivo ni hermosura. Tan cierto es&nbsp; las Sagradas Escrituras lo declaran que el hombre natural no conoce las cosas del Esp&iacute;ritu de Dios, ni las puede entender, porque le son locura.<br \\/><br \\/>Quiz&aacute; algunos de los que me oyen sienten un profundo desprecio hacia el pueblo piadoso&nbsp; &iexcl;est&aacute;n tan cargados de man&iacute;as, tienen escr&uacute;pulos de conciencia por tales nimiedades, parecen siempre tan graves y poco amigos de la diver&not;si&oacute;n!&nbsp; que no pueden soportar su compa&ntilde;&iacute;a.&nbsp;<\\/p>\\\",\\r\\n        \\\"date\\\": \\\"1456228474\\\",\\r\\n        \\\"counter\\\": \\\"0\\\",\\r\\n        \\\"type_id\\\": \\\"2\\\",\\r\\n        \\\"user_id\\\": \\\"1\\\",\\r\\n        \\\"created_at\\\": \\\"2016-02-23 11:54:34\\\",\\r\\n        \\\"updated_at\\\": \\\"2016-02-23 11:54:34\\\",\\r\\n        \\\"images\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"131\\\",\\r\\n            \\\"url\\\": \\\"images\\/posts\\/catedral28.jpg\\\",\\r\\n            \\\"post_id\\\": \\\"109\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:54:34\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:54:34\\\"\\r\\n          },\\r\\n          {\\r\\n            \\\"id\\\": \\\"132\\\",\\r\\n            \\\"url\\\": \\\"images\\/posts\\/catedral27.jpg\\\",\\r\\n            \\\"post_id\\\": \\\"109\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:54:34\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:54:34\\\"\\r\\n          },\\r\\n          {\\r\\n            \\\"id\\\": \\\"133\\\",\\r\\n            \\\"url\\\": \\\"images\\/posts\\/catedral26.jpg\\\",\\r\\n            \\\"post_id\\\": \\\"109\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:54:34\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:54:34\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"videos\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"69\\\",\\r\\n            \\\"url\\\": \\\"https:\\/\\/www.youtube.com\\/v\\/RWu6gyDHTPU\\\",\\r\\n            \\\"post_id\\\": \\\"109\\\",\\r\\n            \\\"youtube_id\\\": \\\"RWu6gyDHTPU\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:54:34\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:54:34\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"user\\\": {\\r\\n          \\\"id\\\": \\\"1\\\",\\r\\n          \\\"name\\\": \\\"gonzalo huanco\\\",\\r\\n          \\\"email\\\": \\\"gonzalo@gmail.com\\\",\\r\\n          \\\"role_id\\\": \\\"1\\\",\\r\\n          \\\"created_at\\\": \\\"2015-12-10 11:24:17\\\",\\r\\n          \\\"updated_at\\\": \\\"2015-12-10 11:24:17\\\"\\r\\n        }\\r\\n      },\\r\\n      {\\r\\n        \\\"id\\\": \\\"108\\\",\\r\\n        \\\"title\\\": \\\"Venid luego, dice Jehov\\u00E1, y estemos a cuenta\\\",\\r\\n        \\\"description\\\": \\\"<p>En la pel&iacute;cula jurassic Park el paleont&oacute;logo de talla mundial Allen Granr, quien ha dedicado su vida al estudio de los dinosaurios, de repente se enfrenta con criaturas prehist&oacute;ricas vivas. Cae a tierra pasmado. Y el porqu&eacute; es obvio. Una cosa era unir pedazos de una imagen bien fundada pero imperfecta de dinosaurios al recolectar f&oacute;siles y huesos. Pero otra cosa era ver un dinosaurio de verdad&rdquo; (Ilustraciones Perfectas sobre todo tema y para toda ocasi&oacute;n. Ed. UNILIT). Creo que as&iacute; tambi&eacute;n en el d&iacute;a del juicio muchos que creen estar bien con Dios, sufrir&aacute;n un tremendo impacto al darse cuenta, que se han quedado cortos en la entrega de su vida a Dios. Pero antes que eso suceda Dios esta diciendo: &ldquo;Venid luego, y estemos a cuenta<\\/p>\\\",\\r\\n        \\\"date\\\": \\\"1456228152\\\",\\r\\n        \\\"counter\\\": \\\"0\\\",\\r\\n        \\\"type_id\\\": \\\"2\\\",\\r\\n        \\\"user_id\\\": \\\"1\\\",\\r\\n        \\\"created_at\\\": \\\"2016-02-23 11:49:12\\\",\\r\\n        \\\"updated_at\\\": \\\"2016-02-23 11:49:12\\\",\\r\\n        \\\"images\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"130\\\",\\r\\n            \\\"url\\\": \\\"images\\/posts\\/catedral28.jpg\\\",\\r\\n            \\\"post_id\\\": \\\"108\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:49:12\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:49:12\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"videos\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"68\\\",\\r\\n            \\\"url\\\": \\\"https:\\/\\/www.youtube.com\\/v\\/0-T49mpKeIc\\\",\\r\\n            \\\"post_id\\\": \\\"108\\\",\\r\\n            \\\"youtube_id\\\": \\\"0-T49mpKeIc\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:49:12\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:49:12\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"user\\\": {\\r\\n          \\\"id\\\": \\\"1\\\",\\r\\n          \\\"name\\\": \\\"gonzalo huanco\\\",\\r\\n          \\\"email\\\": \\\"gonzalo@gmail.com\\\",\\r\\n          \\\"role_id\\\": \\\"1\\\",\\r\\n          \\\"created_at\\\": \\\"2015-12-10 11:24:17\\\",\\r\\n          \\\"updated_at\\\": \\\"2015-12-10 11:24:17\\\"\\r\\n        }\\r\\n      },\\r\\n      {\\r\\n        \\\"id\\\": \\\"107\\\",\\r\\n        \\\"title\\\": \\\"LA MEJOR INVITACION PARA TU VIDA\\\",\\r\\n        \\\"description\\\": \\\"<p>ESTA TARDE QUIERO HABLARTE DE LA MEJOR INVITACION, PORQUE NADIE LA PUEDE SUPERAR, UNA INVITACION QUE PUEDE TRANSFORMAR TU VIDA ENTERA, UNA INVITACION QUE TRAERA A TU VIDA PAZ, QUE TRAERA A TU VIDA BENDICION, QUE TRAERA A TU VIDA SALVACION Y VIDA ETERNA.<\\/p>\\\\r\\\\n<p>NO ES UNA INVITACION CUALQUIERA, ES UNA INVITACION ESPECIAL, UNA INVITACION PERSONAL, UNA INVITACION PREPARADA ESPECIALMENTE PARA TU VIDA DE PARTE DE DIOS.<\\/p>\\\\r\\\\n<p>REFLEXIONEMOS SOBRE ESTE PRECIOSO VERSICULO:<\\/p>\\\\r\\\\n<p>I)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VENID: SI TU QUIERES RECIBIR LO QUE DIOS TIENE PARA TU VIDA, SI QUIERES EXPERIMENTAR SU AMOR, SU MISERICORDIA, SU PERDON, SUS MARAVILLAS, TIENES QUE VENIR A EL.<\\/p>\\\\r\\\\n<p>&nbsp;<\\/p>\\\\r\\\\n<p>POSIBLEMENTE EN TU REBELDIA, EN TU CORAZON LLENO DE ORGULLO, POSIBLEMENTE PIENSES &iquest;Y PORQUE TENGO YO QUE VENIR A EL? Y LA RESPUESTA ES MUY SENCILLA: PORQUE EL YA VINO, EL YA BUSCO AL PECADOR, EL YA MURIO EN LA CRUZ, EL YA LLEVO NUESTROS PECADOS SOBRE EL, EL YA VINO AHORA NOS TOCA A CADA UNO DE NOSOTROS VENIR A EL.<\\/p>\\\\r\\\\n<p>&nbsp;<\\/p>\\\\r\\\\n<p>II)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LUEGO: ESTA INVITACION REQUIERE UNA RESPUESTA INMEDIATA, ESTO ES CUESTION LITERALMENTE DE VIDA O MUERTE, ES ALGO QUE NO PUEDE ESPERAR, DE ESTO DEPENDE TU FUTURO ETERNO, TODO EN LA VIDA QUEDA EN SEGUNDO PLANO CUANDO SE TRATA DE NUESTRO DESTINO ETERNO.<\\/p>\\\\r\\\\n<p>&nbsp;<\\/p>\\\\r\\\\n<p>HOY ES EL DIA QUE DIOS QUIERE DEJARSE ENCONTRAR POR TI, HOY ES EL DIA QUE EL SE&Ntilde;OR QUIERE ESCUCHAR TU VOZ LLAMANDOLO(ISAIAS 55:6)<\\/p>\\\",\\r\\n        \\\"date\\\": \\\"1456228093\\\",\\r\\n        \\\"counter\\\": \\\"0\\\",\\r\\n        \\\"type_id\\\": \\\"2\\\",\\r\\n        \\\"user_id\\\": \\\"1\\\",\\r\\n        \\\"created_at\\\": \\\"2016-02-23 11:48:13\\\",\\r\\n        \\\"updated_at\\\": \\\"2016-02-23 11:48:13\\\",\\r\\n        \\\"images\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"129\\\",\\r\\n            \\\"url\\\": \\\"images\\/posts\\/catedral27.jpg\\\",\\r\\n            \\\"post_id\\\": \\\"107\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:48:13\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:48:13\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"videos\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"67\\\",\\r\\n            \\\"url\\\": \\\"https:\\/\\/www.youtube.com\\/v\\/FBU-vggR2W0\\\",\\r\\n            \\\"post_id\\\": \\\"107\\\",\\r\\n            \\\"youtube_id\\\": \\\"FBU-vggR2W0\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:48:13\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:48:13\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"user\\\": {\\r\\n          \\\"id\\\": \\\"1\\\",\\r\\n          \\\"name\\\": \\\"gonzalo huanco\\\",\\r\\n          \\\"email\\\": \\\"gonzalo@gmail.com\\\",\\r\\n          \\\"role_id\\\": \\\"1\\\",\\r\\n          \\\"created_at\\\": \\\"2015-12-10 11:24:17\\\",\\r\\n          \\\"updated_at\\\": \\\"2015-12-10 11:24:17\\\"\\r\\n        }\\r\\n      },\\r\\n      {\\r\\n        \\\"id\\\": \\\"106\\\",\\r\\n        \\\"title\\\": \\\"Lectura del santo evangelio seg\\u00FAn san Mateo (23,1-12)\\\",\\r\\n        \\\"description\\\": \\\"<p><span style=\\\\\\\"color: #4c4c4c; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 21px;\\\\\\\">En aquel tiempo, Jes&uacute;s habl&oacute; a la gente y a sus disc&iacute;pulos, diciendo: &laquo;En la c&aacute;tedra de Mois&eacute;s se han sentado los escribas y los fariseos: haced y cumplid lo que os digan; pero no hag&aacute;is lo que ellos hacen, porque ellos no hacen lo que dicen. Ellos l&iacute;an fardos pesados e insoportables y se los cargan a la gente en los hombros, pero ellos no est&aacute;n dispuestos a mover un dedo para empujar. Todo lo que hacen es para que los vea la gente: alargan las filacterias y ensanchan las franjas del manto; les gustan los primeros puestos en los banquetes y los asientos de honor en las sinagogas; que les hagan reverencias por la calle y que la gente los llame maestros. Vosotros, en cambio, no os dej&eacute;is llamar maestro, porque uno solo es vuestro maestro, y todos vosotros sois hermanos. Y no llam&eacute;is padre vuestro a nadie en la tierra, porque uno solo es vuestro Padre, el del cielo. No os dej&eacute;is llamar consejeros, porque uno solo es vuestro consejero, Cristo. El primero entre vosotros ser&aacute; vuestro servidor. El que se enaltece ser&aacute; humillado, y el que se humilla ser&aacute; enaltecido.&raquo;<\\/span><\\/p>\\\",\\r\\n        \\\"date\\\": \\\"1456227737\\\",\\r\\n        \\\"counter\\\": \\\"0\\\",\\r\\n        \\\"type_id\\\": \\\"2\\\",\\r\\n        \\\"user_id\\\": \\\"1\\\",\\r\\n        \\\"created_at\\\": \\\"2016-02-23 11:42:17\\\",\\r\\n        \\\"updated_at\\\": \\\"2016-02-23 11:42:17\\\",\\r\\n        \\\"images\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"128\\\",\\r\\n            \\\"url\\\": \\\"images\\/posts\\/catedral26.jpg\\\",\\r\\n            \\\"post_id\\\": \\\"106\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:42:17\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:42:17\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"videos\\\": [\\r\\n          {\\r\\n            \\\"id\\\": \\\"66\\\",\\r\\n            \\\"url\\\": \\\"https:\\/\\/www.youtube.com\\/v\\/RWu6gyDHTPU\\\",\\r\\n            \\\"post_id\\\": \\\"106\\\",\\r\\n            \\\"youtube_id\\\": \\\"RWu6gyDHTPU\\\",\\r\\n            \\\"created_at\\\": \\\"2016-02-23 11:42:17\\\",\\r\\n            \\\"updated_at\\\": \\\"2016-02-23 11:42:17\\\"\\r\\n          }\\r\\n        ],\\r\\n        \\\"user\\\": {\\r\\n          \\\"id\\\": \\\"1\\\",\\r\\n          \\\"name\\\": \\\"gonzalo huanco\\\",\\r\\n          \\\"email\\\": \\\"gonzalo@gmail.com\\\",\\r\\n          \\\"role_id\\\": \\\"1\\\",\\r\\n          \\\"created_at\\\": \\\"2015-12-10 11:24:17\\\",\\r\\n          \\\"updated_at\\\": \\\"2015-12-10 11:24:17\\\"\\r\\n        }\\r\\n      }\\r\\n    ]\\r\\n  }\\r\\n}\";\n";


        try{
            if(result != null){

            JSONObject jsonObject = new JSONObject(result);
            // Verificar el estado del resultado de la busqueda
            String status = jsonObject.getString("status");

            if( status.equals("ok") ){
                // capturar el objeto data principal
                JSONObject objectDataPrincipal = jsonObject.getJSONObject("data");
                Data objetoData = new Data();
                objetoData = gson.fromJson(objectDataPrincipal.toString(),Data.class);

                JSONArray dataItems = new JSONArray(objectDataPrincipal.getString("data"));
                for(int i=0; i < dataItems.length();i++){
                    valueData = dataItems.getJSONObject(i);
                    // preguntar si es una publicacion o un evento
                    int tipoPublicación=0;
                    try{
                        tipoPublicación = valueData.getInt("type_id");

                    }catch(Exception ex){
                        ex.printStackTrace();
                    }

                    if(tipoPublicación==0){
                        // es un evento
                        ValueDataEvento valueDataEvento = new ValueDataEvento();
                        valueDataEvento = gson.fromJson(valueData.toString(), ValueDataEvento.class);
                        listaData.add(valueDataEvento);
                    }else{
                        //es una reflexion, sermon o noticia
                        ValueDataPublicacion valueDataPublicacion = new ValueDataPublicacion();
                        valueDataPublicacion = gson.fromJson(valueData.toString(),ValueDataPublicacion.class);
                        listaData.add(valueDataPublicacion);

                    }

                }
                // la listaData copiar a la lista de objeto objetoData
                objetoData.setData(listaData);

                if(objetoData != null){
                    // Lanzar activity para mostrar un listView con el resultado de la búsqueda
                    Intent intent = new Intent(this.activity, ResultadoBusquedaActivity.class);
                    intent.putExtra("objetoData",objetoData);
                    intent.putExtra("title",this.title);

                    this.activity.startActivity(intent);
                }
            }else{
                mostrarDialogAlert("Resultado", "No se encontraron resultados para la búsqueda");
            }
            }else{
                mostrarDialogAlert("Error", "No se puede establecer conexión con el servidor");
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }

        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    /**
     * Lectura de contenido recuperado
     * del webservice
     * @param URL
     * @return
     */
    public String readJSONFeed(String URL) throws JSONException {

        if (!this.palabra.equalsIgnoreCase("")) {
            URL = URL + "?title=" + this.palabra + "&more_read=true";
        }
        StringBuilder stringBuilder = new StringBuilder();
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(URL);
        httpPost.setHeader("content-type", "application/json");
        JSONObject dato = new JSONObject();
        //dato.put("title", this.palabra);//https://catedraldelafe.herokuapp.com/api/search?title=del&more_read=true

        //dato.put("more_read",true);
        try{
            HttpResponse response = httpClient.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if(statusCode == 200){
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while((line = reader.readLine()) != null){
                    stringBuilder.append(line);
                }
                inputStream.close();

            }
        } catch (Exception ex) {
            Log.i(TAG, "No se pudo establecer la conexión HTTP con el servidor");
            ex.printStackTrace();
            return null;
        }
        return stringBuilder.toString();
    }


    public void mostrarDialogAlert(String title, String message){
        alertDialog = new AlertDialog.Builder(this.activity).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
