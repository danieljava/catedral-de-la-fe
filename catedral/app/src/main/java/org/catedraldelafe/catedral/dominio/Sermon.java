package org.catedraldelafe.catedral.dominio;

import java.util.ArrayList;

/**
 * Created by pchacon.
 */
public class Sermon {

    private long id;
    private String title;
    private String description;
    private ArrayList<Image> images;
    private ArrayList<Video> videos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Sermon(long id, String titulo, String description, ArrayList<Image> images, ArrayList<Video> videos) {
        this.id = id;
        this.title = titulo;
        this.description = description;
        this.images = images;
        this.videos = videos;
    }

    public Sermon(){
    }
}
