package org.catedraldelafe.catedral;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.catedraldelafe.catedral.util.Configuration;


/**
 * Created by dduran on 18/09/2015.
 */
public class PedidoOracionActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private EditText txtNombre, txtOracion;
    private Button btnEnviar;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_oracion);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //coloco la barra navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        txtNombre = (EditText)findViewById(R.id.TxtNombre);
        txtOracion = (EditText)findViewById(R.id.TxtOracion);
        btnEnviar = (Button) findViewById(R.id.BtnEnviar);
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                String oracion = txtOracion.getText().toString();

                String extraText = nombre + " - " + oracion;

                Context context=getApplicationContext();

                //String to = context.getString(R.string.email);
                String to = Configuration.getProperty("pedido.oracion.email", context);

                String emailAddressList[] = {to};
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, emailAddressList);
                i.putExtra(Intent.EXTRA_SUBJECT, "Pedido de Oración");
                i.putExtra(Intent.EXTRA_TEXT   , extraText);

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(PedidoOracionActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

                Dialog dialog = new Dialog(PedidoOracionActivity.this);
                dialog.setTitle("Email enviado!");
                dialog.show();

                txtNombre = (EditText)findViewById(R.id.TxtNombre);
                txtNombre.setText("");
                txtOracion = (EditText)findViewById(R.id.TxtOracion);
                txtOracion.setText("");
            }
        });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            intent = new Intent(this, OracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_noticias) {
            intent = new Intent(this, NoticiaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sermonSemanal){
            intent = new Intent(this, SermonesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_busqueda){
            MainActivity ac = new MainActivity();
            ac.crearDialogBusqueda(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
