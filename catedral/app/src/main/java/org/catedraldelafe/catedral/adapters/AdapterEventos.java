package org.catedraldelafe.catedral.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.catedraldelafe.catedral.R;
import org.catedraldelafe.catedral.dominio.Evento;

import java.util.ArrayList;

/**
 * Created by mateo on 07/01/2016.
 */
public class AdapterEventos extends BaseAdapter {
//declaro variables
    private Activity activity;
    private ArrayList<Evento> items;

    public AdapterEventos(Activity activity, ArrayList<Evento> items) {
        this.activity = activity;
        this.items = items;
    }



    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //definicion de variables
        View v = convertView;
        // relacionamos el listview con nuestro archivo xml itemlista

            if (convertView==null){
                LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inf.inflate(R.layout.item_evento_individual, null);
            }

        // se colocan los datos del evento  a cada item
        Evento evento = items.get(position);
        //colocar la fecha
        TextView fecha = (TextView)v.findViewById(R.id.textViewEspacio);
        fecha.setText(evento.getFecha());
        //colocar la descripcion
        TextView descripcion = (TextView)v.findViewById(R.id.textViewDescripcion);
        descripcion.setText(evento.getDescripcion());
        //colocar la titulo
        TextView titulo = (TextView)v.findViewById(R.id.textViewTituloSeleccionado);
        titulo.setText(evento.getTitulo());
        //colocar el estado del evento
        TextView estado = (TextView)v.findViewById(R.id.textView2);
        estado.setText(evento.getEstado());


        //retornamos la vista
        return v;
    }
}
