package org.catedraldelafe.catedral.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.catedraldelafe.catedral.listeners.Listener;
import org.catedraldelafe.catedral.util.Configuration;
import org.catedraldelafe.catedral.util.Constants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


/**
 * Created by pablo on 18/09/2015.
 */
public class ClientNoticias extends AsyncTask<Void,Void,String> {
        private ProgressDialog dialog;
        public Listener delegate = null;
        private Activity activity;
        //ruta del servidor
        private String urlNoticias;
        private Context ctx;


        public ClientNoticias(Context context, Listener resp, Activity act){
                ctx = context;
                delegate = resp;
                urlNoticias = Configuration.getProperty("url.base", context) + Configuration.getProperty("ws.noticias", context);
                this.activity = act;
        }

        /**
         * Metodo que se conecta al RESTFUL para obtener un resultado
         * */
        public String getRestFul()
        {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(urlNoticias);
                String strResultado="NaN";
                String resultImages="";
                String resultVideos="";
                try {
                        //ejecuta
                        HttpResponse response = httpclient.execute(httppost);
                        //Obtiene la respuesta del servidor
                        String jsonResult = inputStreamToString(response.getEntity().getContent()).toString();

                        JSONObject object = new JSONObject(jsonResult);
                        //obtiene el status
                        String status = object.getString("status");
                        //ok -> todo esta bien
                        if( status.equals("ok") )
                        {
                                strResultado="";
                                //extrae los registros
                                JSONObject object1 = object.getJSONObject("data");
                                JSONArray array = object1.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++)
                                {
                                        //recorre cada registro y concatena el resultado
                                        JSONObject row = array.getJSONObject(i);
                                        String titulo = row.getString("title");
                                        String descripcion = row.getString("description");
                                        descripcion = descripcion.replaceAll("\n", "");

                                        JSONArray arrayImages = row.getJSONArray("images");
                                        for (int j = 0; j < arrayImages.length(); j++)
                                        {
                                                //recorre cada registro y concatena el resultado
                                                JSONObject imagen = arrayImages.getJSONObject(j);
                                                String url = imagen.getString("url");
                                                if (j != arrayImages.length() - 1) resultImages += url + " #@ ";
                                                else resultImages += url;
                                        }

                                        JSONArray arrayVideos = row.getJSONArray("videos");
                                        for (int k = 0; k < arrayVideos.length(); k++)
                                        {
                                                //recorre cada registro y concatena el resultado
                                                JSONObject video = arrayVideos.getJSONObject(k);
                                                String urlVideo = video.getString("url");
                                                String idVideo = video.getString("youtube_id");
                                                urlVideo = urlVideo + "*" + idVideo;
                                                if (k != arrayVideos.length() - 1) resultVideos += urlVideo + " #@ ";
                                                else resultVideos += urlVideo;
                                        }

                                        strResultado += titulo + " #& " + descripcion + " #& " + resultImages + " #& " + resultVideos + "\n";

                                        resultImages = "";
                                        resultVideos = "";
                                }

                                return strResultado;
                        }

                } catch (ClientProtocolException e) {
                        strResultado = null;
                        e.printStackTrace();
                } catch (IOException e) {
                        strResultado = null;
                        e.printStackTrace();
                } catch (JSONException e) {
                        strResultado = null;
                        e.printStackTrace();
                } catch (Exception e){
                        strResultado = null;
                        e.printStackTrace();
                }

                return strResultado;
        }

        /**
         * Transforma el InputStream en un String
         * @return StringBuilder
         * */
        private StringBuilder inputStreamToString(InputStream is) throws UnsupportedEncodingException {
                String line = "";
                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader rd = new BufferedReader( new InputStreamReader(is, Charset.forName("ISO-8859-1")) );
                try
                {
                        while( (line = rd.readLine()) != null )
                        {
                                stringBuilder.append(line);
                        }
                }
                catch( IOException e)
                {
                        e.printStackTrace();
                }

                return stringBuilder;
        }


        /**
         * Realiza la tarea en segundo plano
         * */

        @Override
        protected String doInBackground(Void... params) {
                String resul = getRestFul();
                 return resul;
        }

        @Override
        protected void onPostExecute(String result) {
                if(result != null)
                        delegate.resultadoOK(result);
                else
                        delegate.resultadoError(result);
                if (dialog.isShowing()) {
                        dialog.dismiss();
                }
        }

        @Override
        protected void onPreExecute() {
                dialog = new ProgressDialog(activity);
                dialog.setTitle(Constants.MESSAGE_PROCESANDO);
                dialog.setMessage(Constants.MESSAGE_WAIT);
                dialog.setIndeterminate(true);
                dialog.setCancelable(false);
                dialog.show();
                super.onPreExecute();
        }

}
