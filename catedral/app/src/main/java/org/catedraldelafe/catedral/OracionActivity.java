package org.catedraldelafe.catedral;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.ListView;

import org.catedraldelafe.catedral.adapters.AdapterOraciones;
import org.catedraldelafe.catedral.tasks.ClienteOraciones;
import org.catedraldelafe.catedral.listeners.Listener;
import org.catedraldelafe.catedral.dominio.Oracion;
import org.catedraldelafe.catedral.util.Configuration;

import java.util.ArrayList;

public class OracionActivity extends AppCompatActivity implements Listener, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oracion);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //coloco la barra navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // llamada al cliente eventos
        ClienteOraciones client = new ClienteOraciones(getApplicationContext(), OracionActivity.this,OracionActivity.this);
        client.execute();
    }


    public void resultadoOK(String response) {
        System.out.println("## RESULT = " + response);
        String [] rows = response.split("\\n");  //separa la cadena de caracteres cuando encuentra el \n
        System.out.println("## LOONG ARRAY = " + rows.length);
        // coloco los datos en en list view
        ListView lista = (ListView)findViewById(R.id.listaOracion);
        final ArrayList<Oracion> arrayOracion = new ArrayList<Oracion>();
       Oracion oracion;

        /////   preguntar si el arrayOracion tiene datos  /////

        //coloco los datos en el objeto y agrego a la lista
        for (int i = 0; i < rows.length; i++) {
            String tit1 = rows[i];
            //busco dentro de tit1
            String []rows1 = tit1.split("---");
            String htmlTextStr = Html.fromHtml(rows1[1]).toString();
            oracion = new Oracion(rows1[2], rows1[3], Configuration.getProperty("url.base", this)+rows1[0], htmlTextStr);
            arrayOracion.add(oracion);
        }
        // creo un adapter personalizado
        AdapterOraciones adapterEventos = new AdapterOraciones(this, arrayOracion);
        lista.setAdapter(adapterEventos);
    }

    @Override
    public void resultadoError(String response) {
        //---alert dialog ------------
        AlertDialog.Builder dlgErrorSermon = new AlertDialog.Builder(this);
        dlgErrorSermon.setTitle("Atención");
        dlgErrorSermon.setMessage("Revise su conexión a Internet");
        //dlgExitNroMesa.setIcon(R.drawable.icon_info24);
        dlgErrorSermon.setCancelable(false);
        dlgErrorSermon.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlgExitNroMesa, int id) {
                Intent intent = new Intent(OracionActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        dlgErrorSermon.show();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            intent = new Intent(this, OracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_noticias) {
            intent = new Intent(this, NoticiaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sermonSemanal){
            intent = new Intent(this, SermonesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_busqueda){
            MainActivity ac = new MainActivity();
            ac.crearDialogBusqueda(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
