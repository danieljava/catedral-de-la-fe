package org.catedraldelafe.catedral;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.CalendarContract;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.catedraldelafe.catedral.adapters.BusquedaAdapter;
import org.catedraldelafe.catedral.dominio.Data;
import org.catedraldelafe.catedral.dominio.ValueDataEvento;
import org.catedraldelafe.catedral.dominio.ValueDataPublicacion;
import org.catedraldelafe.catedral.tasks.ClientBusqueda;
import org.catedraldelafe.catedral.util.Configuration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



/**
 * created by caroapaza
 */
public class ResultadoBusquedaActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "ResultadoBusquedaAc";
    private Activity activity;
    private Data objetoData = null;
    private ValueDataPublicacion valueDataPublicacion;
    private ValueDataEvento valueDataEvento;
    private Button first;
    private Button last;
    private Button prev;
    private Button next;
    private TextView textViewDisplaying;
    public final static int MI_REQUEST_CODE = 1;
    private  ListView listView;
    private AlertDialog alertDialog;
    private  BusquedaAdapter busquedaAdapter;
    private List<String> urls;
    private int lastposition=0;
    private ProgressDialog dialog;
    private Handler mImagesProgressHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {

            busquedaAdapter = new BusquedaAdapter(activity, objetoData.getData());
            // busquedaAdapter.setNotifyOnChange(true);

            listView.setAdapter(busquedaAdapter);
            busquedaAdapter.notifyDataSetChanged();

            //listView.smoothScrollToPosition(lastposition);
            listView.setSelection(lastposition);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }


        }
    };

    private Handler dialoghandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {

            if(dialog==null) {

                dialog = new ProgressDialog(activity);
                dialog.setMessage("Espere ...");
                dialog.setIndeterminate(true);
                dialog.show();
            }else if(!dialog.isShowing()){
                dialog.show();

            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitybusqueda);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //coloco la barra navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        activity = this;

        Gson gson = new Gson();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        /*
        textViewDisplaying = (TextView) findViewById(R.id.displaying);

        first = (Button) findViewById(R.id.buttonfirst);
        prev = (Button) findViewById(R.id.buttonprev);
        next = (Button) findViewById(R.id.buttonnext);
        last = (Button) findViewById(R.id.buttonlast);
        */
        urls = new ArrayList<String>();
        objetoData = (Data)getIntent().getSerializableExtra("objetoData");

         listView = (ListView)findViewById(R.id.lvBusqueda);
        TextView textview = (TextView) findViewById(R.id.textviewtitulobusqueda);
        textview.setText((String)getIntent().getSerializableExtra("title"));
         busquedaAdapter = new BusquedaAdapter(activity, objetoData.getData());
        listView.setAdapter(busquedaAdapter);

        String pagina = String.valueOf(objetoData.getCurrent_page());
        Log.i(TAG,"CURRENT PAGE -> "+pagina);
        Log.i(TAG,"LAST PAGE -> "+ objetoData.getLast_page());
        Log.i(TAG, "TOTAL -> " + objetoData.getTotal());
        //textViewDisplaying.setText("Pág. " + pagina);

        Integer total = objetoData.getTotal();

        //updateButtons();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                Object objData = objetoData.getData().get(position);
                if (objData instanceof ValueDataPublicacion) {
                    valueDataPublicacion = (ValueDataPublicacion) objData;
                    // segun el tipo de resultado mostrar el layout que corresponda.

                    switch (valueDataPublicacion.getType_id()) {
                        case 1: {//refexion diaria = lectura del día??????
                            intent = new Intent(activity, ReflexionIndividualActivity.class);
                            intent.putExtra("reflexion", valueDataPublicacion);
                            startActivity(intent);
                            break;
                        }
                        case 2: {//sermon semanal
                            intent = new Intent(activity, SermonSemanalActivity.class);
                            intent.putExtra("sermon", valueDataPublicacion);
                            startActivity(intent);

                            break;
                        }
                        case 3: {//Noticias
                            intent = new Intent(activity, NoticiaIndividualActivity.class);
                            intent.putExtra("noticia", valueDataPublicacion);
                            startActivity(intent);
                            break;
                        }
                    }
                } else if (objData instanceof ValueDataEvento) {
                    valueDataEvento = (ValueDataEvento) objData;

                    //saco la hora y la fecha del objeto seleccionado
                    // String FechaHora = arrayEventos.get(position).getFechaHora();
                    String FechaHora = valueDataEvento.getDate_start();
                    Long dateMilisegundo = null;
                    dateMilisegundo = dateMilisegundo.valueOf(FechaHora);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy hh mm");
                    String dateString = formatter.format(new Date(dateMilisegundo));
                    // busco y separo de la cadena hora, minutos, dia, mes año para colocar en el calendario...
                    String[] rowFechaHora = dateString.split(" ");
                    // coloco los datos en el calendario
                    Calendar cal = Calendar.getInstance();


                    cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(rowFechaHora[0]));
                    cal.set(Calendar.MONTH, Integer.parseInt(rowFechaHora[1]));
                    cal.set(Calendar.YEAR, Integer.parseInt(rowFechaHora[2]));
                    cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(rowFechaHora[3]));
                    cal.set(Calendar.MINUTE, Integer.parseInt(rowFechaHora[4]));
                    Intent intent1 = new Intent(Intent.ACTION_EDIT);
                    intent1.setType("vnd.android.cursor.item/event");
                    //asigno a la actividad datos iniciales....descripcion, titulo, fecha  --- falta revisar --
                    intent1.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, cal.getTimeInMillis());
                    intent1.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, cal.getTimeInMillis() + 60 * 60 * 1000);
                    intent1.putExtra(CalendarContract.Events.ALL_DAY, false);
                    intent1.putExtra(CalendarContract.Events.RRULE, "FREQ=DAILY;COUNT=1");
                    intent1.putExtra(CalendarContract.Events.TITLE, valueDataEvento.getTitle());
                    intent1.putExtra(CalendarContract.Events.DESCRIPTION, valueDataEvento.getDescription());
                    intent1.putExtra(CalendarContract.Events.EVENT_LOCATION, "Calle: cilo123456");
                    // poner un recordatorio
                    intent1.putExtra(CalendarContract.Reminders.MINUTES, 15);
                    intent1.putExtra(CalendarContract.Reminders.EVENT_ID, CalendarContract.Events._ID);
                    intent1.putExtra(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                    startActivityForResult(intent1, MI_REQUEST_CODE);

                }
            }
        });

        listView.setOnScrollListener(new OnScrollListener() {


            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (listView.getAdapter() == null)
                    return;

                if (listView.getAdapter().getCount() == 0)
                    return;

                int l = visibleItemCount + firstVisibleItem;
                if (l >= totalItemCount) {
                    lastposition=firstVisibleItem;
                    //loadData();



                   /* runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //loadData();

//stuff that updates ui

                        }
                    });*/
                        //Looper.prepare();
                    Thread t = new Thread(new Runnable() {
                        public void run() {
                                loadData();



                        }
                    });
                    t.start();


                   //Looper.loop();



                }
            }
        });


    }

    public void loadData() {

        // send your request
        // receive it by callback or asynctask
        String result = null;
        Gson gson = new Gson();
        JSONObject valueData = null;

        List<Object> listaData = new ArrayList<Object>();



        try{
            String url=objetoData.getNext_page_url();
            if (url!=null&&url!=""&&!urls.contains(url)) {

                android.os.Message message = dialoghandler.obtainMessage();

                Bundle bundle = new Bundle();
                bundle.putString("key", "your string message");

                message.setData(bundle);

                dialoghandler.sendMessage(message);

                urls.add(url);
                result = readJSONFeed(url);//https://catedraldelafe.herokuapp.com/api/search
            }
        }catch (JSONException jsone){
            jsone.printStackTrace();
        }

        try{
            if(result != null){

                for (int i=0; i<objetoData.getData().size();i++){

                    listaData.add(objetoData.getData().get(i));

                }

                JSONObject jsonObject = new JSONObject(result);
                // Verificar el estado del resultado de la busqueda
                String status = jsonObject.getString("status");

                if( status.equals("ok") ){
                    // capturar el objeto data principal
                    JSONObject objectDataPrincipal = jsonObject.getJSONObject("data");
                    Data objetoDatalocal = gson.fromJson(objectDataPrincipal.toString(),Data.class);

                    JSONArray dataItems = new JSONArray(objectDataPrincipal.getString("data"));
                    for(int i=0; i < dataItems.length();i++){
                        valueData = dataItems.getJSONObject(i);
                        // preguntar si es una publicacion o un evento
                        int tipoPublicación=0;
                        try{
                            tipoPublicación = valueData.getInt("type_id");

                        }catch(Exception ex){
                            ex.printStackTrace();
                        }

                        if(tipoPublicación==0){
                            // es un evento
                            ValueDataEvento valueDataEvento = new ValueDataEvento();
                            valueDataEvento = gson.fromJson(valueData.toString(), ValueDataEvento.class);
                            listaData.add(valueDataEvento);
                        }else{
                            //es una reflexion, sermon o noticia
                            ValueDataPublicacion valueDataPublicacion = new ValueDataPublicacion();
                            valueDataPublicacion = gson.fromJson(valueData.toString(),ValueDataPublicacion.class);
                            listaData.add(valueDataPublicacion);

                        }

                    }
                    // la listaData copiar a la lista de objeto objetoData


                    objetoDatalocal.setData(listaData);

                    objetoData=objetoDatalocal;


                    if(objetoData != null){

                        android.os.Message message = mImagesProgressHandler.obtainMessage();

                        Bundle bundle = new Bundle();
                        bundle.putString("key", "your string message");

                        message.setData(bundle);

                        mImagesProgressHandler.sendMessage(message);




                    }
                }else{
                    mostrarDialogAlert("Resultado", "No se encontraron resultados para la búsqueda");
                }
            }else{
                mostrarDialogAlert("Error", "No se puede establecer conexión con el servidor");
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }




    }

    public void mostrarDialogAlert(String title, String message){

    }


    public String readJSONFeed(String URL) throws JSONException {
        StringBuilder stringBuilder = new StringBuilder();
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(URL);
        httpPost.setHeader("content-type", "application/json");
        JSONObject dato = new JSONObject();
        //dato.put("title", this.palabra);//https://catedraldelafe.herokuapp.com/api/search?title=del&more_read=true

        //dato.put("more_read",true);
        try{
            HttpResponse response = httpClient.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if(statusCode == 200){
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while((line = reader.readLine()) != null){
                    stringBuilder.append(line);
                }
                inputStream.close();

            }
        } catch (Exception ex) {
            Log.i(TAG, "No se pudo establecer la conexión HTTP con el servidor");
            ex.printStackTrace();
            return null;
        }
        return stringBuilder.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Verificamos ques sea la respuesta que solicitamos
        if (requestCode == MI_REQUEST_CODE) {
            // Verificamos que la subtarea mi request code terminó y se actuliza el layout
            System.out.println("******************* CALENDARIO TERMINO CON EXITO***************");
            finish();
            startActivity(getIntent());

        }
    }


    private void updateButtons()
    {
        if (getCurrentPage() > 1)
        {
            first.setEnabled(true);
            prev.setEnabled(true);
        }
        else
        {
            first.setEnabled(false);
            prev.setEnabled(false);
        }
        if (getCurrentPage() < getLastPage())
        {
            next.setEnabled(true);
            last.setEnabled(true);
        }
        else
        {
            next.setEnabled(false);
            last.setEnabled(false);
        }

    }

    private int getLastPage()
    {
      return objetoData.getLast_page();
    }

    private int getCurrentPage()
    {
      return objetoData.getCurrent_page();
    }

    /******************** BUTTONS *************************/

    public void first(View v)
    {// ir a ClientBusquedala pagina 1 inicial
        //new ClientBusqueda(ResultadoBusquedaActivity.this,null,0).execute(Configuration.getProperty("url.base", context) + Configuration.getProperty("ws.busqueda", context) + "?page=1");
    }

    public void next(View v)
    {// ir a la siguiente pagina
        //new ClientBusqueda(ResultadoBusquedaActivity.this,null,0).execute(objetoData.getNext_page_url());
    }

    public void previous(View v)
    {// ir a la pagina anterior
        //new ClientBusqueda(ResultadoBusquedaActivity.this,null,0).execute(objetoData.getPrev_page_url());
    }

    public void last(View v)
    {// ir a la ultima pagina
        double paginaFinal = 0;
        Log.i(TAG,"objetoData es -> "+objetoData.getTotal());
        int resto = objetoData.getTotal() % 3;
        Log.i(TAG,"El resto es -> "+resto);
        if(resto > 0){
            double division = objetoData.getTotal() / 3f;
            paginaFinal = Math.ceil(division);
            Log.i(TAG, "el valor de pagina final el " + paginaFinal);
            //new ClientBusqueda(ResultadoBusquedaActivity.this,null,0).execute(Configuration.getProperty("url.base", context) + Configuration.getProperty("ws.busqueda", context) + "?page=" + paginaFinal);
            //deshabilitar todos los botones next y last
            textViewDisplaying.setText("Pág. " + paginaFinal);
            next.setEnabled(false);
            last.setEnabled(false);
        }else{
            paginaFinal = objetoData.getTotal() / 3;
            Log.i(TAG, "el valor de pagina final el " + paginaFinal);
            textViewDisplaying.setText("Pág. " + paginaFinal);
            next.setEnabled(false);
            last.setEnabled(false);
            first.setEnabled(true);
            prev.setEnabled(true);
            //new ClientBusqueda(ResultadoBusquedaActivity.this,null,0).execute(Configuration.getProperty("url.base", context) + "api/search?page=" + paginaFinal);
            //deshabilitar todos los botones next y last

        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_lecturaDia) {
            new ClientBusqueda(activity,"",0,"Lectura del Dia").execute(Configuration.getProperty("url.base", activity) + "api/posts?type=1");

        } else if (id == R.id.nav_noticias) {
            new ClientBusqueda(activity,"",0,"Noticias").execute(Configuration.getProperty("url.base", activity) + "api/posts?type=3");

        } else if (id == R.id.nav_sermonSemanal){
            new ClientBusqueda(activity,"",0,"Sermón Semanal").execute(Configuration.getProperty("url.base", activity) + "api/posts?type=2");

        }
        else if (id == R.id.nav_busqueda){

            //crearDialogBusqueda(activity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



}
