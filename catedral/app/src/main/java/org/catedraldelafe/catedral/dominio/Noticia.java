package org.catedraldelafe.catedral.dominio;

/**
 * Created by dduran on 15/01/2016.
 */
public class Noticia {

    private long id;
    private String titulo;
    private String descripcion;
    private String[] imagenes;
    private String[]videos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String[] getImagenes() {
        return imagenes;
    }

    public void setImagenes(String[] imagenes) {
        this.imagenes = imagenes;
    }

    public String[] getVideos() {
        return videos;
    }

    public void setVideos(String[] videos) {
        this.videos = videos;
    }

    public Noticia(long id, String titulo, String descripcion, String[] imagenes, String[] videos) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagenes = imagenes;
        this.videos = videos;
    }

    public Noticia(){

    }
}
