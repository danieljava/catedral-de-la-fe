package org.catedraldelafe.catedral;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import org.catedraldelafe.catedral.adapters.AdapterNoticias;
import org.catedraldelafe.catedral.tasks.ClientNoticias;
import org.catedraldelafe.catedral.listeners.Listener;
import org.catedraldelafe.catedral.dominio.Noticia;

import java.util.ArrayList;

/**
 * Created by dduran on 18/09/2015.
 */
public class NoticiaActivity extends AppCompatActivity implements  Listener, NavigationView.OnNavigationItemSelectedListener {

    private ArrayList<Noticia> arrayNoticias = new ArrayList<Noticia>();
    private ListView lista;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticia);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //coloco la barra navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lista = (ListView) findViewById(R.id.listaNoticias);

        //llamado a cliente de web service
        ClientNoticias client = new ClientNoticias(getApplicationContext(), NoticiaActivity.this, NoticiaActivity.this);
        client.execute();


    }


    @Override
    public void resultadoOK(String response) {
        //parseo los datos devueltos por el web service
        String [] rows = response.split("\\n");

        for (String datosServicio: rows){
            if (datosServicio.length() > 0){
                Noticia noticia = new Noticia();
                String [] datos = datosServicio.split("#&");
                noticia.setTitulo(datos[0]);
                String htmlTextStr = Html.fromHtml(datos[1]).toString();
               // noticia.setDescripcion(datos[1]);
                noticia.setDescripcion(htmlTextStr);
                String imagenes = datos[2];
                String [] imagenesVector = imagenes.split("#@");
                String [] imagenesNew = new String [imagenesVector.length];
                for (int i=0; i < imagenesVector.length; i++){
                    imagenesNew[i] = imagenesVector[i].trim();
                }
                noticia.setImagenes(imagenesNew);
                String videos = datos[3];
                String [] videosVector = videos.split("#@");
                String [] videosNew = new String [videosVector.length];
                for (int j=0; j < videosVector.length; j++){
                    videosNew[j] = videosVector[j].trim();
                }
                noticia.setVideos(videosNew);

                arrayNoticias.add(noticia);
            }
        }

        AdapterNoticias adapter = new AdapterNoticias(this, arrayNoticias);
        lista.setAdapter(adapter);
    }

    @Override
    public void resultadoError(String response) {
        //---alert dialog ------------
        AlertDialog.Builder dlgErrorSermon = new AlertDialog.Builder(this);
        dlgErrorSermon.setTitle("Atención");
        dlgErrorSermon.setMessage("Revise su conexión a Internet");
        //dlgExitNroMesa.setIcon(R.drawable.icon_info24);
        dlgErrorSermon.setCancelable(false);
        dlgErrorSermon.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlgExitNroMesa, int id) {
                Intent intent = new Intent(NoticiaActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        dlgErrorSermon.show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            intent = new Intent(this, OracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_noticias) {
            intent = new Intent(this, NoticiaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sermonSemanal){
            intent = new Intent(this, SermonesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_busqueda){
            MainActivity ac = new MainActivity();
            ac.crearDialogBusqueda(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
