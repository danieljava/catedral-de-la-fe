package org.catedraldelafe.catedral.dominio;

/**
 * Created by mateo on 07/01/2016.
 */
public class Oracion {
    private  long id;
    private String autor;
    private String fecha;
    private String imagen;
    private String descripcion;

    public Oracion(String autor, String fecha, String imagen, String descripcion) {
        this.autor = autor;
        this.fecha = fecha;
        this.imagen = imagen;
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
