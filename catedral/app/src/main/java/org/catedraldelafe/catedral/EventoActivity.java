package org.catedraldelafe.catedral;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.catedraldelafe.catedral.adapters.AdapterEventos;
import org.catedraldelafe.catedral.tasks.ClienteEventos;
import org.catedraldelafe.catedral.dominio.Evento;
import org.catedraldelafe.catedral.listeners.Listener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class EventoActivity extends AppCompatActivity implements Listener, NavigationView.OnNavigationItemSelectedListener {
    private Context context;
    private static final String TAG = "MainActivity";
    private Activity activity = this;
    public final static int MI_REQUEST_CODE = 1;


    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //coloco la barra navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        context = getApplicationContext();
        // llamada al cliente eventos
        ClienteEventos client = new ClienteEventos(getApplicationContext(), EventoActivity.this, EventoActivity.this);
        client.execute();
    }




    public void resultadoOK(String response) {
        System.out.println("## RESULT = " + response);
        String [] rows = response.split("\\n");  //separa la cadena de caracteres cuando encuentra el \n
        System.out.println("## LOONG ARRAY = " + rows.length);
        // coloco los datos en en list view
        ListView lista = (ListView)findViewById(R.id.listaEventos);
        final ArrayList<Evento> arrayEventos = new ArrayList<Evento>();
        Evento evento;
        String estado = "SEGUIR EVENTO";
        //coloco los datos en el objeto y agrego a la lista
        for (int i = 0; i < rows.length; i++) {
            String tit1 = rows[i];
            //busco dentro de tit1
            String []rows1 = tit1.split(" - ");
            // por cada evento.. determino si ya estan en la bd de android y determino cual estado corresponde.
            int band = 0;
            Cursor cursor = context.getContentResolver()
                    .query(
                            Uri.parse("content://com.android.calendar/events"),
                            new String[]{"calendar_id", "title", "description",
                                    "dtstart", "dtend", "eventLocation"}, null,
                            null, null);

            if (cursor.moveToFirst()) {


                do {
                    long id = cursor.getLong(0);
                    String displayName = cursor.getString(1);
                    if(displayName.equals(rows1[0])) {
                        System.out.println("******************* nombre del evento encontrado:****************" + displayName );
                        estado= "EVENTO SEGUIDO";
                      if (band==0){
                        evento = new Evento(rows1[3], rows1[2], rows1[0], rows1[1], estado);
                        arrayEventos.add(evento);
                          band = 1;
                      }
                    }

                } while (cursor.moveToNext());
            }


            // los agrego en la lista con su estado que corresponde
            if(band==0){
                System.out.println("******************* sin evento:****************");
                evento = new Evento(rows1[3], rows1[2], rows1[0], rows1[1], "SEGUIR EVENTO");
                arrayEventos.add(evento);}

        }
        // creo un adapter personalizado
        final AdapterEventos adapterEventos = new AdapterEventos(this, arrayEventos);
        lista.setAdapter(adapterEventos);
        // cuando selecciona un item de la lista voy a mostrar su posicion y llamar a otra activity : calendar provider
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //saco la hora y la fecha del objeto seleccionado
                String FechaHora = arrayEventos.get(position).getFechaHora();
                Long dateMilisegundo = null;
                dateMilisegundo = dateMilisegundo.valueOf(FechaHora);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy hh mm");
                String dateString = formatter.format(new Date(dateMilisegundo));
                // busco y separo de la cadena hora, minutos, dia, mes año para colocar en el calendario...
                String[] rowFechaHora = dateString.split(" ");
                // coloco los datos en el calendario
                Calendar cal = Calendar.getInstance();


                cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(rowFechaHora[0]));
                cal.set(Calendar.MONTH, Integer.parseInt(rowFechaHora[1]));
                cal.set(Calendar.YEAR, Integer.parseInt(rowFechaHora[2]));
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(rowFechaHora[3]));
                cal.set(Calendar.MINUTE, Integer.parseInt(rowFechaHora[4]));
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                //asigno a la actividad datos iniciales....descripcion, titulo, fecha  --- falta revisar --
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, cal.getTimeInMillis());
                intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, cal.getTimeInMillis() + 60 * 60 * 1000);
                intent.putExtra(CalendarContract.Events.ALL_DAY, false);
                intent.putExtra(CalendarContract.Events.RRULE, "FREQ=DAILY;COUNT=1");
                intent.putExtra(CalendarContract.Events.TITLE, arrayEventos.get(position).getTitulo());
                intent.putExtra(CalendarContract.Events.DESCRIPTION, arrayEventos.get(position).getDescripcion());
                intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "Calle: cilo123456");
                // poner un recordatorio
                intent.putExtra(CalendarContract.Reminders.MINUTES, 15);
                intent.putExtra(CalendarContract.Reminders.EVENT_ID, CalendarContract.Events._ID);
                intent.putExtra(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                startActivityForResult(intent, MI_REQUEST_CODE);

            }
        });
    }

    @Override
    public void resultadoError(String response) {
        //---alert dialog ------------
        AlertDialog.Builder dlgErrorSermon = new AlertDialog.Builder(this);
        dlgErrorSermon.setTitle("Atención");
        dlgErrorSermon.setMessage("Revise su conexión a Internet");
        //dlgExitNroMesa.setIcon(R.drawable.icon_info24);
        dlgErrorSermon.setCancelable(false);
        dlgErrorSermon.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlgExitNroMesa, int id) {
                Intent intent = new Intent(EventoActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        dlgErrorSermon.show();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            intent = new Intent(this, OracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_noticias) {
            intent = new Intent(this, NoticiaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sermonSemanal){
            intent = new Intent(this, SermonesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_busqueda){
            MainActivity ac = new MainActivity();
            ac.crearDialogBusqueda(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


// este metodo sirve para cuando las subactividades finalizan y capturar su resultado
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Verificamos ques sea la respuesta que solicitamos
        if (requestCode == MI_REQUEST_CODE) {
            // Verificamos que la subtarea mi request code terminó y se actuliza el layout
            System.out.println("******************* CALENDARIO TERMINO CON EXITO***************");
            finish();
            startActivity(getIntent());

        }
    }
}

