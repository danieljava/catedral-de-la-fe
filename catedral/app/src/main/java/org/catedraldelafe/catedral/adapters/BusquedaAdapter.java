package org.catedraldelafe.catedral.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;

import org.catedraldelafe.catedral.R;
import org.catedraldelafe.catedral.dominio.Image;
import org.catedraldelafe.catedral.dominio.ValueDataEvento;
import org.catedraldelafe.catedral.dominio.ValueDataPublicacion;
import org.catedraldelafe.catedral.util.Configuration;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by CaroApaza on 27/01/2016.
 */
public class BusquedaAdapter extends ArrayAdapter {
    private Context context;
    private List<Object> datos;
    private URL imageUrl = null;
    private static final String TAG = "BusquedaAdapter";
    private SmartImageView smartImageView;
    private TextView description;

    public BusquedaAdapter(Context context, List<Object> datos){

        super(context, 0,datos);
        this.context = context;
        this.datos = datos;
    }



    @Override
    public int getItemViewType(int position) {

        int tipoLayout = 0;
        if(datos.get(position) instanceof ValueDataPublicacion){
            tipoLayout = 1;
        }else if(datos.get(position) instanceof ValueDataEvento){
            tipoLayout = 2;
        }
        return tipoLayout;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

       // if (convertView == null) {

            int type = getItemViewType(position);
            convertView = getInflatedLayoutForType(type);
            if(type == 1){//setear datos al layout de publicaciones
                smartImageView = (SmartImageView)convertView.findViewById(R.id.smartImage);


                ValueDataPublicacion valueDataPublicacion = new ValueDataPublicacion();
                description = (TextView) convertView.findViewById(R.id.txtDescripcionsermon);

                valueDataPublicacion = (ValueDataPublicacion)datos.get(position);
                TextView titulo = (TextView) convertView.findViewById(R.id.txtTitulo);
                TextView tipoPublicacion = (TextView)convertView.findViewById(R.id.txtTipoPublicacion);
                Color mColor = new Color();
                switch((valueDataPublicacion.getType_id())){
                    case 1:{
                        tipoPublicacion.setText("Reflexión Diaria");
                        convertView.setBackgroundColor(Color.rgb(204, 0, 68));
                        titulo.setTextColor(Color.rgb(255, 255, 255));

                        break;
                    }
                    case 2:{
                        tipoPublicacion.setText("Sermón Semanal");
                        titulo.setTextColor(Color.rgb(255, 255, 255));
                        convertView.setBackgroundColor(Color.rgb(184, 77, 255));

                        //
                        break;}
                    case 3: {
                        tipoPublicacion.setText("Noticias");
                        convertView.setBackgroundColor(Color.rgb(255, 255, 255));

                        description.setVisibility(View.GONE);
                        break;
                    }
                }


                titulo.setText(valueDataPublicacion.getTitle());
                String descriptiontext = valueDataPublicacion.getDescription();
                String htmlTextStr = Html.fromHtml(descriptiontext).toString();
                description.setText(htmlTextStr);

                List<Image> listaImagenes = valueDataPublicacion.getImages();
                String url = null;

                if(listaImagenes.size()>0){
                  url = Configuration.getProperty("url.base", context) + listaImagenes.get(0).getUrl();
                  // url = listaImagenes.get(0).getUrl();
                   smartImageView.setImageUrl(url,null);
                }else{


                    //ocultar el control smartImage, si el resultado de la busqueda no tiene imagenes
                    //smartImageView.setVisibility(View.GONE);


                }

                if(smartImageView.getDrawable()==null){

                    switch((valueDataPublicacion.getType_id())) {
                        case 1:
                            smartImageView.setVisibility(View.GONE);
                            break;
                        case 2:
                            smartImageView.setVisibility(View.GONE);
                            break;
                        case 3: {

                            InputStream is = getClass().getResourceAsStream("/res/drawable/noticiasdefaultimage.png");

                            smartImageView.setImageDrawable(Drawable.createFromStream(is, ""));
                            break;
                        }
                    }

                }

                TextView fecha = (TextView) convertView.findViewById(R.id.txtFecha);
                String date = valueDataPublicacion.getDate();
                if(date != null && !date.isEmpty()){
                    Long dateMilisegundo = null;
                    dateMilisegundo = dateMilisegundo.valueOf(date);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    date = formatter.format(new Date(dateMilisegundo));
                    fecha.setText(date);
                }else{
                    fecha.setText("");
                }

            }else if(type == 2){// setear datos al layout de eventos
                ValueDataEvento valueDataEvento = new ValueDataEvento();
                valueDataEvento = (ValueDataEvento)datos.get(position);

                TextView fecha = (TextView)convertView.findViewById(R.id.textViewEspacio);
                String dateStart = valueDataEvento.getDate_start();
                Long dateMilisegundo = null;
                if(dateStart==null){
                    dateStart="0";

                }else  if(dateStart.equalsIgnoreCase("")) {
                    dateStart = "0";
                }
                dateMilisegundo = dateMilisegundo.valueOf(dateStart);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMMMM");
                dateStart = formatter.format(new Date(dateMilisegundo));
                fecha.setText(dateStart.toUpperCase());

                TextView descripcion = (TextView)convertView.findViewById(R.id.textViewDescripcion);
                descripcion.setText(valueDataEvento.getDescription());

                TextView titulo = (TextView)convertView.findViewById(R.id.textViewTituloSeleccionado);
                titulo.setText(valueDataEvento.getTitle());
            }
       // }

        return convertView;
    }




    private View getInflatedLayoutForType(int type) {
        if (type == 1) {// publicacion
            return LayoutInflater.from(getContext()).inflate(R.layout.content_item, null);
        } else if (type == 2) {//evento
            return LayoutInflater.from(getContext()).inflate(R.layout.item_evento_individual, null);
        } else {
            return null;
        }
    }




}
