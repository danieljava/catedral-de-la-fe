package org.catedraldelafe.catedral;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;

public class SermonActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sermon);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int densityDpi = metrics.densityDpi;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Bundle bundle = getIntent().getExtras();

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.gravity= Gravity.CENTER;

        LinearLayout.LayoutParams paramSmall = new LinearLayout.LayoutParams(160, 160);
        paramSmall.gravity=Gravity.CENTER;

        LinearLayout.LayoutParams paramLand = new LinearLayout.LayoutParams(1250, 650);
        paramLand.gravity=Gravity.CENTER;


        //seteo el titulo
        TextView titulo = new TextView(this);
        titulo.setGravity(Gravity.CENTER);
        titulo.setPadding(0, 10, 20, 0);
        titulo.setText(bundle.getString("TITULO"));
        titulo.setTextColor(Color.parseColor("#000000"));
        titulo.setLayoutParams(param);

        //seteo el detalle
        TextView detalle = new TextView(this);
        detalle.setGravity(Gravity.CENTER);
        detalle.setPadding(0, 10, 20, 0);

        detalle.setText(bundle.getString("DETALLE"));
        detalle.setLayoutParams(param);

        LinearLayout ly = (LinearLayout) findViewById(R.id.listaDetalle);

        SmartImageView image = new SmartImageView(this);
        image.setClickable(false);
        image.setAdjustViewBounds(true);

        if (densityDpi <= 120){
            image.setLayoutParams(paramSmall);
        } else if ((densityDpi > 200) && (densityDpi <=320)){
            image.setLayoutParams(paramLand);
        } else {
            image.setLayoutParams(param);
        }

        //seteo la imagen
        Rect rect = new Rect(image.getLeft(), image.getTop(), image.getRight(), image.getBottom());
        try{
            image.setImageUrl(bundle.getString("IMAGEN"), rect);
        }catch (Exception e){
            e.getStackTrace();
            image.setBackgroundResource(R.drawable.catedral1);
        }
        //image.setBackgroundResource(R.drawable.catedral1);

        ly.addView(image);
        ly.addView(titulo);
        ly.addView(detalle);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            intent = new Intent(this, OracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_noticias) {
            intent = new Intent(this, NoticiaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sermonSemanal){
            intent = new Intent(this, SermonesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_busqueda){
            MainActivity ac = new MainActivity();
            ac.crearDialogBusqueda(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
