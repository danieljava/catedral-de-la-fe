package org.catedraldelafe.catedral.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.catedraldelafe.catedral.dominio.Sermon;
import org.catedraldelafe.catedral.listeners.IListenerSermon;
import org.catedraldelafe.catedral.util.Configuration;
import org.catedraldelafe.catedral.util.Constants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by pablo.
 */
public class ClientSermones extends AsyncTask<Void,Void,ArrayList<Sermon>> {
        private ProgressDialog dialog;
        private Activity activity;
        public IListenerSermon delegate;

        //ruta del servidor
        private String urlSermones;
        private Context ctx;

        public ClientSermones(Context context, IListenerSermon resp, Activity activity){
                ctx = context;
                delegate = resp;
                urlSermones = Configuration.getProperty("url.base", context) + Configuration.getProperty("ws.sermones", context);
                this.activity = activity;
        }
        /**
         * Metodo que se conecta al RESTFUL para obtener un resultado
         * */
        public ArrayList<Sermon> getRestFul()
        {
                Gson gson = new Gson();

                /*
                        SETEO EL TIMEOUT DEL CLIENTE
                 */
                HttpParams httpParameters = new BasicHttpParams();
                // Set the timeout in milliseconds until a connection is established.
                // The default value is zero, that means the timeout is not used.
                int timeoutConnection = 2000;
                HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                // Set the default socket timeout (SO_TIMEOUT)
                // in milliseconds which is the timeout for waiting for data.
                int timeoutSocket = 2000;
                HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

                HttpClient httpclient = new DefaultHttpClient(httpParameters);
                HttpPost httppost = new HttpPost(urlSermones);
                ArrayList<Sermon> result = new ArrayList<Sermon>();
                try {

                        HttpResponse response = httpclient.execute(httppost);
                        String jsonResult = EntityUtils.toString(response.getEntity());

                        JSONObject object = new JSONObject(jsonResult);
                        String status = object.getString("status");

                        if( status.equals("ok") ) {
                                //extrae los registros
                                JSONObject object1 = object.getJSONObject("data");
                                JSONArray array = object1.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                        Sermon sermon = new Sermon();
                                        sermon = gson.fromJson(String.valueOf(array.getJSONObject(i)), Sermon.class);

                                        result.add(sermon);
                                }
                                return result;
                        }
                } catch (ClientProtocolException e) {
                        result = null;
                        e.printStackTrace();
                } catch (IOException e) {
                        result = null;
                        e.printStackTrace();
                } catch (JSONException e) {
                        result = null;
                        e.printStackTrace();
                } catch (Exception e){
                        result = null;
                        e.printStackTrace();
                }
                return result;
        }

        /**
         * Realiza la tarea en segundo plano
         * */

        @Override
        protected ArrayList<Sermon> doInBackground(Void... params) {
                ArrayList result = getRestFul();
                 return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Sermon> result) {
                if(result != null){
                        delegate.resultSermonOk(result);
                }else{
                        delegate.resultSermonError();
                }
                if (dialog.isShowing()) {
                        dialog.dismiss();
                }
        }

        @Override
        protected void onPreExecute() {
                dialog = new ProgressDialog(activity);
                dialog.setTitle(Constants.MESSAGE_PROCESANDO);
                dialog.setMessage(Constants.MESSAGE_WAIT);
                dialog.setIndeterminate(true);
                dialog.setCancelable(false);
                dialog.show();

                super.onPreExecute();
        }
}
