package org.catedraldelafe.catedral;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * created by caroapaza
 */
public class BusquedaDialogActivity extends AppCompatActivity {
    private Context context;
    private Activity mainActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_busqueda_dialog);
        context = getApplicationContext();
        mainActivity = (Activity)getIntent().getSerializableExtra("activity");
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View v = inflater.inflate(R.layout.activity_busqueda_dialog, null);
        alertDialog.setView(v);
        alertDialog.create();

        alertDialog.setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
            Gson gson = new Gson();
            List<Object> listaData = new ArrayList<Object>();
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText palabraBuscar = (EditText) v.findViewById(R.id.etxtBusqueda);
                String palabra;
                palabra = palabraBuscar.getText().toString();
                if (!palabra.isEmpty()) {
                    int page = 1;
                  //  new ClientBusqueda(mainActivity, palabra, page).execute("http://192.168.0.157:90/api/search");//definitiva
/*try {
                        String contenido = readJSONAssets();
                        try{
                            JSONObject jsonObject = new JSONObject(contenido);
                            // Verificar el estado del resultado de la búsqueda
                            String status = jsonObject.getString("status");

                            if( status.equals("ok") ){
                                // capturar el objeto data principal
                                JSONObject objectDataPrincipal = jsonObject.getJSONObject("data");
                                Data objetoData = new Data();
                                //Gson gson = new Gson();
                                objetoData = gson.fromJson(objectDataPrincipal.toString(),Data.class);

                                JSONArray dataItems = new JSONArray(objectDataPrincipal.getString("data"));
                                for(int i=0; i < dataItems.length();i++){
                                      valueData = dataItems.getJSONObject(i);
                                    // preguntar si es una publicacion o un evento
                                    String tipoPublicación = valueData.getString("tipo_publicacion");
                                    if(tipoPublicación.equals("publicaciones")){
                                        //es una reflexion, sermon o noticia
                                        ValueDataPublicacion valueDataPublicacion = new ValueDataPublicacion();
                                        valueDataPublicacion = gson.fromJson(valueData.toString(),ValueDataPublicacion.class);
                                        Log.i(TAG,valueDataPublicacion.toString());
                                        listaData.add(valueDataPublicacion);

                                    }else if(tipoPublicación.equals("eventos")){
                                        // es un evento
                                        ValueDataEvento valueDataEvento = new ValueDataEvento();
                                        Log.i(TAG, valueData.toString());
                                        valueDataEvento = gson.fromJson(valueData.toString(), ValueDataEvento.class);
                                        listaData.add(valueDataEvento);
                                        //Toast.makeText(context,"es un evento",Toast.LENGTH_LONG).show();
                                    }

                                }
                                // la listaData copiar a la lista de objeto objetoData
                                objetoData.setData(listaData);


                                if(objetoData != null){
                                    // Lanzar activity para mostrar un listView con el resultado de la búsqueda
                                    Intent intent = new Intent(context, ResultadoBusquedaActivity.class);
                                    intent.putExtra("objetoData",objetoData);
                                    startActivity(intent);

                                }
                            }else{
                               mostrarDialogAlert("Resultado", "No se obtuvieron resultados para la búsqueda");
                            }


                        }catch(Exception ex){
                            ex.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                } else {
                    Toast.makeText(context, "Debe ingresar texto a buscar", Toast.LENGTH_LONG).show();
                }

            }

        });


        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context, "Cancelando", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                }

        );

        alertDialog.show();






    }

}
