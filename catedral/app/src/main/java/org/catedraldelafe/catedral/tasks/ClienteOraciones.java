package org.catedraldelafe.catedral.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.catedraldelafe.catedral.listeners.Listener;
import org.catedraldelafe.catedral.util.Configuration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mateo on 14/01/2016.
 */


public class ClienteOraciones extends AsyncTask<Void,Void,String> {
    private ProgressDialog dialog;
    private Activity activity;
    public Listener delegate = null;
   private String urlOraciones;
   private Context ctx;


    public ClienteOraciones(Context context, Listener resp, Activity activity){
        ctx = context;
        delegate = resp;
        this.activity=activity;
        urlOraciones = Configuration.getProperty("url.base", context) + Configuration.getProperty("ws.pedidos.oracion", context);
    }

    /**
     * Metodo que se conecta al RESTFUL para obtener un resultado
     * */
    public String getRestFul()
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlOraciones);
        String strResultado="NaN";
        try {
            //ejecuta
            HttpResponse response = httpclient.execute(httppost);
            //Obtiene la respuesta del servidor
            String jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject object = new JSONObject(jsonResult);
            //obtiene el status
            String status = object.getString("status");
            //ok -> todo esta bien
            if( status.equals("ok") )
            {
                strResultado="";
                //extrae los registros
                JSONObject object1 = object.getJSONObject("data");
                JSONArray array = object1.getJSONArray("data");
                for (int i = 0; i < array.length(); i++)
                {
                    //recorre cada registro y concatena el resultado
                    JSONObject row = array.getJSONObject(i);
                    String fecha = row.getString("date");
                    String descripcion = row.getString("description");
                    String autor = row.getJSONObject("user").getString("name"); // para acceder a un objeto en el json {} solo es llave
                    String imagen = row.getJSONArray("images").getJSONObject(0).getString("url");  //para acceder a un array  en json [] con corchete
                   //transformar los milisegundos
                    Long dateMilisegundo = null;
                    dateMilisegundo = dateMilisegundo.valueOf(fecha);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMMMMMMMM yyyy");
                    String dateString = formatter.format(new Date(dateMilisegundo));


                    strResultado += imagen + "---" + descripcion + "---" + autor + "---" + dateString.toUpperCase() + "\n";
                    System.out.println("## cadena concatenada = " + strResultado);

                }

                return strResultado;
            }

        } catch (ClientProtocolException e) {
            strResultado = null;
            e.printStackTrace();
        } catch (IOException e) {
            strResultado = null;
            e.printStackTrace();
        } catch (JSONException e) {
            strResultado = null;
            e.printStackTrace();
        }

        return strResultado;
    }

    /**
     * Transforma el InputStream en un String
     * @return StringBuilder
     * */
    private StringBuilder inputStreamToString(InputStream is) throws UnsupportedEncodingException {
        String line = "";
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader rd = new BufferedReader( new InputStreamReader(is, Charset.forName("ISO-8859-1")) );
        try
        {
            while( (line = rd.readLine()) != null )
            {
                stringBuilder.append(line);
            }
        }
        catch( IOException e)
        {
            e.printStackTrace();
        }

        return stringBuilder;
    }


    /**
     * Realiza la tarea en segundo plano
     * */

    @Override
    protected String doInBackground(Void... params) {
        String resul = getRestFul();
        return resul;
    }

    @Override
    protected void onPostExecute(String result) {
        if(result != null)
            delegate.resultadoOK(result);
        else
            delegate.resultadoError(result);
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(activity);
        dialog.setTitle("Procesando");
        dialog.setMessage("Espere unos segundos ...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();
        super.onPreExecute();
    }

}


