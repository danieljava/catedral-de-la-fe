package org.catedraldelafe.catedral.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;

import org.catedraldelafe.catedral.R;
import org.catedraldelafe.catedral.dominio.Oracion;
import org.catedraldelafe.catedral.tasks.RoundedCornersSmartImageView;

import java.util.ArrayList;

/**
 * Created by mateo on 07/01/2016.
 */
public class AdapterOraciones extends BaseAdapter {
//declaro variables
    private Activity activity;
    private ArrayList<Oracion> items;
    SmartImageView imagen;

    public AdapterOraciones(Activity activity, ArrayList<Oracion> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //definicion de variables
        View v = convertView;
        // relacionamos el listview con nuestro archivo xml itemlista

            if (convertView==null){
                LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inf.inflate(R.layout.item_oracion_individual, null);
            }

        // se colocan los datos del evento  a cada item
        Oracion oracion = items.get(position);
        //colocar la fecha
    //    TextView fecha = (TextView)v.findViewById(R.id.textViewFechaOracion);
      //  fecha.setText(oracion.getAutor());
        //colocar la descripcion
        TextView descripcion = (TextView)v.findViewById(R.id.textViewLecturaDiaria);
        descripcion.setText(oracion.getDescripcion());
        //colocar la fecha
        TextView fecha = (TextView)v.findViewById(R.id.textViewFecha);
        fecha.setText(oracion.getFecha());
        //colocar la titulo
        TextView titulo = (TextView)v.findViewById(R.id.textViewAutor);
        titulo.setText(oracion.getAutor());
        // colocar una imagen redonda
        RoundedCornersSmartImageView thumb_image = (RoundedCornersSmartImageView) v.findViewById(R.id.myImagen);
        thumb_image. setImageUrl (oracion.getImagen(), null);
        thumb_image. setRadius (350);
        System.out.println("## url de las imagenes en el adapter= " + oracion.getImagen());
        //retornamos la vista
        return v;
    }
}
