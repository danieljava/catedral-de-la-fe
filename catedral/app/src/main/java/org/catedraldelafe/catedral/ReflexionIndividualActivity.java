package org.catedraldelafe.catedral;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.github.snowdream.android.widget.SmartImageView;
import org.catedraldelafe.catedral.adapters.VideoAdapter;
import org.catedraldelafe.catedral.dominio.Image;
import org.catedraldelafe.catedral.dominio.ValueDataPublicacion;
import org.catedraldelafe.catedral.dominio.Video;
import org.catedraldelafe.catedral.util.Configuration;
import org.catedraldelafe.catedral.util.Constants;
import java.util.ArrayList;
import java.util.List;

/**
 * created by caroapaza
 */
public class ReflexionIndividualActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private TextView txtTitulo, txtDescripcion, txtFecha;
    private LinearLayout linearLayout;
    private Context context;
    private List<Video> videos = new ArrayList<Video>();
    private String urlVideo;
    private static final String TAG = "RefleIndivActivity";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reflexion_individual);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;

        //coloco la barra navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int densityDpi = metrics.densityDpi;

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float scaleFactor = metrics.density;

        float widthDp = widthPixels / scaleFactor;
        float heightDp = heightPixels / scaleFactor;

        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;
        // cantidad de pixeles por pulgada
        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;
        //medida de la diagonal de pantalla, por pitagoras
        double diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));


        ValueDataPublicacion reflexion = (ValueDataPublicacion) getIntent().getSerializableExtra("reflexion");
        linearLayout = (LinearLayout)findViewById(R.id.linearLayoutReflexion2);
        txtTitulo = (TextView)  findViewById(R.id.txtTituloRef);
        txtDescripcion = (TextView) findViewById(R.id.txtDescripcion);

        String tituloCaps = reflexion.getTitle().toUpperCase();
        txtTitulo.setText(tituloCaps);

        String desc = reflexion.getDescription();
        String htmlTextStr = Html.fromHtml(desc).toString();
        txtDescripcion.setText(htmlTextStr);

        //imagenes
        String url = null;
        LinearLayout linearLayoutImagenes = (LinearLayout)findViewById(R.id.linearLayoutImages);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        param.gravity= Gravity.CENTER;
        param.setMargins(2,2,2,2);

        if(diagonalInches >= 2 && diagonalInches < 4){
            //small
            param.width = 150;
            param.height = 150;
        } else if (diagonalInches >= 4 && diagonalInches <= 4.5) {
            //normal
            param.width = 350;
            param.height = 350;
        }else if (diagonalInches > 4.5 && diagonalInches <= 7){
            //large
            param.width = 450;
            param.height = 450;
        }else if(diagonalInches > 7 && diagonalInches <= 10){
            //extra large
            Log.i(TAG,"DISPOSITIVO TABLET DE 10 -> "+diagonalInches);
            param.width = 550;
            param.height = 550;
        }

        if(reflexion.getImages().size() > 0){

            for(int i = 0 ; i < reflexion.getImages().size() ; i++){
                SmartImageView smartImageView = new SmartImageView(context);
                smartImageView.setLayoutParams(param);
                url = null;
                url = Configuration.getProperty("url.base", context)+"/" + reflexion.getImages().get(i).getUrl();
                //url = reflexion.getImages().get(i).getUrl();

                smartImageView.setImageUrl(url, null);
                linearLayoutImagenes.addView(smartImageView);
            }

        }

        //videos
        videos = reflexion.getVideos();
        LinearLayout ly = (LinearLayout)findViewById(R.id.linearLayoutVideos);

        String titulo = getResources().getString(R.string.strTituloVideos);
        TextView txtTitulo = (TextView) findViewById(R.id.txtTituloVideos);
        txtTitulo.setText(titulo);
        videos = reflexion.getVideos();
        if(videos.size() > 0){
            ListView lstVideos = (ListView)findViewById(R.id.lstVideos);
            lstVideos.setAdapter(new VideoAdapter(this,videos));

            lstVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, int position,
                                        long arg) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videos.get(position).getUrl())));

                }

            });
        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            intent = new Intent(this, OracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_noticias) {
            intent = new Intent(this, NoticiaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sermonSemanal){
            intent = new Intent(this, SermonesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_busqueda){
            MainActivity ac = new MainActivity();
            ac.crearDialogBusqueda(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



}
