package org.catedraldelafe.catedral.dominio;



import java.io.Serializable;
import java.util.List;

/**
 * Created by caroapaza on 25/01/2016.
 */
public class ValueDataPublicacion implements Serializable{
    private int id;
    private String title;
    private String description;
    private String date;
    private int counter;
    private int type_id;
    private int user_id;
    private String created_at;
    private String updated_at;
    private List<Image> images;
    private List<Video> videos;
    private Tipo type;
    private String tipo_publicacion;

    public ValueDataPublicacion(){

    }

    public ValueDataPublicacion(int counter, String created_at, String date, String description,
                                int id, List<Image> images, String tipo_publicacion,
                                String title, Tipo type, int type_id, String updated_at,
                                int user_id, List<Video> videos) {
        this.counter = counter;
        this.created_at = created_at;
        this.date = date;
        this.description = description;
        this.id = id;
        this.images = images;
        this.tipo_publicacion = tipo_publicacion;
        this.title = title;
        this.type = type;
        this.type_id = type_id;
        this.updated_at = updated_at;
        this.user_id = user_id;
        this.videos = videos;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {

        return date;
    }

    public void setDate(String date) {

        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public List<Video> getVideos() {
        return videos;
    }


    public String getTipo_publicacion() {
        return tipo_publicacion;
    }

    public void setTipo_publicacion(String tipo_publicacion) {
        this.tipo_publicacion = tipo_publicacion;
    }

    public Tipo getType() {
        return type;
    }

    public void setType(Tipo type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ValueDataPublicacion{" +
                "counter=" + counter +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", type_id=" + type_id +
                ", user_id=" + user_id +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", images=" + images +
                ", videos=" + videos +
                ", type=" + type +
                ", tipo_publicacion='" + tipo_publicacion + '\'' +
                '}';
    }
}
