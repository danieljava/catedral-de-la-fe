package org.catedraldelafe.catedral.dominio;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by caroapaza on 04/02/2016.
 */
public class ValueDataEvento implements Serializable {
    private int id;
    private String date_start;
    private String date_end;
    private String title;
    private String description;
    private int user_id;
    private String created_at;
    private String updated_at;
    private List<Ctag> tags;
    private String tipo_publicacion;


    public ValueDataEvento(){}

    public ValueDataEvento(String created_at, String date_end, String date_start,
                           String description, int id, List<Ctag> tags,
                           String tipo_publicacion, String title,
                           String updated_at, int user_id) {
        this.created_at = created_at;
        this.date_end = date_end;
        this.date_start = date_start;
        this.description = description;
        this.id = id;
        this.tags = tags;
        this.tipo_publicacion = tipo_publicacion;
        this.title = title;
        this.updated_at = updated_at;
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getDate_start() {

        return date_start;
    }

    public void setDate_start(String date_start) {

        this.date_start = date_start;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Ctag> getTags() {
        return tags;
    }

    public void setTags(List<Ctag> tags) {
        this.tags = tags;
    }

    public String getTipo_publicacion() {
        return tipo_publicacion;
    }

    public void setTipo_publicacion(String tipo_publicacion) {
        this.tipo_publicacion = tipo_publicacion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }


    @Override
    public String toString() {
        return "ValueDataEvento{" +
                "created_at='" + created_at + '\'' +
                ", id=" + id +
                ", date_start='" + date_start + '\'' +
                ", date_end='" + date_end + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", user_id=" + user_id +
                ", updated_at='" + updated_at + '\'' +
                ", tags=" + tags +
                ", tipo_publicacion='" + tipo_publicacion + '\'' +
                '}';
    }
}
