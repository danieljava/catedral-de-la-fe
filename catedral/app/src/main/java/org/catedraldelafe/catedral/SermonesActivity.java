package org.catedraldelafe.catedral;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import org.catedraldelafe.catedral.adapters.AdapterSermon;
import org.catedraldelafe.catedral.dominio.Sermon;
import org.catedraldelafe.catedral.listeners.IListenerSermon;
import org.catedraldelafe.catedral.tasks.ClientSermones;
import org.catedraldelafe.catedral.listeners.Listener;

import java.util.ArrayList;

public class SermonesActivity extends AppCompatActivity implements IListenerSermon, NavigationView.OnNavigationItemSelectedListener {
    private ArrayList<Sermon> arraySermones;
    private ListView lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sermones);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lista = (ListView) findViewById(R.id.listaSermones);

        //llamado a cliente de web service
        ClientSermones client = new ClientSermones(getApplicationContext(), SermonesActivity.this, SermonesActivity.this);
        client.execute();

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            intent = new Intent(this, OracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_noticias) {
            intent = new Intent(this, NoticiaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sermonSemanal){
            intent = new Intent(this, SermonesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_busqueda){
            MainActivity ac = new MainActivity();
            ac.crearDialogBusqueda(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void resultSermonOk(ArrayList<Sermon> response) {
        AdapterSermon adapter = new AdapterSermon(this, response);
        lista.setAdapter(adapter);
    }

    @Override
    public void resultSermonError() {

        //---alert dialog de salir-------------
        AlertDialog.Builder dlgErrorSermon = new AlertDialog.Builder(this);
        dlgErrorSermon.setTitle("Atención");
        dlgErrorSermon.setMessage("Revise su conexión a Internet");
        //dlgExitNroMesa.setIcon(R.drawable.icon_info24);
        dlgErrorSermon.setCancelable(false);
        dlgErrorSermon.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlgExitNroMesa, int id) {
                Intent intent = new Intent(SermonesActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        dlgErrorSermon.show();

    }
}
