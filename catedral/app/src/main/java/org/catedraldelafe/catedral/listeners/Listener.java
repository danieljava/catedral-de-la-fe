package org.catedraldelafe.catedral.listeners;

/**
 * Created by mateo on 14/01/2016.
 */
public interface Listener {
    public void resultadoOK(String response);
    public void resultadoError(String response);
}