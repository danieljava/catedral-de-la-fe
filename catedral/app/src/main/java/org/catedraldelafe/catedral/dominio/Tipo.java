package org.catedraldelafe.catedral.dominio;

import java.io.Serializable;

/**
 * Created by caroapaza on 25/01/2016.
 */
public class Tipo implements Serializable{
    private int id;
    private String name;
    private String description;
    private String created_at;
    private String updated_at;


    public Tipo(){

    }

    public Tipo(String created_at, String description, int id, String name, String updated_at) {
        this.created_at = created_at;
        this.description = description;
        this.id = id;
        this.name = name;
        this.updated_at = updated_at;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "Tipo{" +
                "created_at='" + created_at + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }
}
