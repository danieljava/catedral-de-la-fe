package org.catedraldelafe.catedral.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.github.snowdream.android.widget.SmartImageView;

import org.catedraldelafe.catedral.R;
import org.catedraldelafe.catedral.dominio.Video;

import java.util.List;

/**
 * Created by caroapaza on 18/02/2016.
 */
public class VideoAdapter extends BaseAdapter {
    private List<Video> list;
    private Context context;

    public VideoAdapter(Context context, List<Video> list){

        this.context = context;
        this.list = list;
    }



    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        String vid;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_video, parent, false);
        }
        SmartImageView video = (SmartImageView)v.findViewById(R.id.siVideo);

        video.setAdjustViewBounds(true);
        Rect rect = new Rect(video.getLeft(), video.getTop(), video.getRight(), video.getBottom());
        vid = this.list.get(position).getUrl();
        String youtube_id = this.list.get(position).getYoutube_id();
        if(youtube_id != null){
            Log.i("VIDEO id --->",youtube_id);
        }else{
            Log.i("VIDEO","NO TIENE ID DEL VIDEO");
        }
        try{
            String urlImgVideo = "http://img.youtube.com/vi/" + youtube_id + "/0.jpg";
            video.setImageUrl(urlImgVideo, rect);

        }catch (Exception e){
            Log.i("VideoAdapter", "error ");
            e.printStackTrace();
        }

        return v;
    }
}
