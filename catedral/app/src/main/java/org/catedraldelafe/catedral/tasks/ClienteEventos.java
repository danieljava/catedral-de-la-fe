package org.catedraldelafe.catedral.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.catedraldelafe.catedral.listeners.Listener;
import org.catedraldelafe.catedral.util.Configuration;
import org.catedraldelafe.catedral.util.Constants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mateo on 14/01/2016.
 */


public class ClienteEventos extends AsyncTask<Void,Void,String> {
    private ProgressDialog dialog;
    private Activity activity;
    public Listener delegate = null;
    private String urlEvento;

   // private final String HTTP_RESTFUL="http://b184b7f7.ngrok.io/api/events"; //accediendo a la url desde internet con ngrok
    private Context ctx;

    public ClienteEventos(Context context, Listener resp, Activity activity){
        ctx = context;
        delegate = resp;
        this.activity=activity;
        urlEvento = Configuration.getProperty("url.base", context) + Configuration.getProperty("ws.evento", context);
    }

    /**
     * Metodo que se conecta al RESTFUL para obtener un resultado
     * */
    public String getRestFul()
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlEvento);
        String strResultado="NaN";
        try {
            //ejecuta
            HttpResponse response = httpclient.execute(httppost);
            //Obtiene la respuesta del servidor
            String jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject object = new JSONObject(jsonResult);
            //obtiene el status
            String status = object.getString("status");
            //ok -> todo esta bien
            if( status.equals("ok") ) {
                strResultado="";
                //extrae los registros
                JSONObject object1 = object.getJSONObject("data");
                JSONArray array = object1.getJSONArray("data");

                for (int i = 0; i < array.length(); i++)
                {
                    //recorre cada registro y concatena el resultado
                    JSONObject row = array.getJSONObject(i);
                    String titulo = row.getString("title");
                    String descripcion = row.getString("description");

                    //TODO: NO HARDCODE DATE
                    //String date = row.getString("date");
                    String date = "12345678";

                    //transforma los milisegundos
                    Long dateMilisegundo = null;
                    dateMilisegundo = dateMilisegundo.valueOf(date);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM");
                    String dateString = formatter.format(new Date(dateMilisegundo));
                    strResultado += titulo + " - " + descripcion + " - " + dateString.toUpperCase() + " - " + date +"\n";
                }
                return strResultado;
            }

        } catch (ClientProtocolException e) {
            strResultado = null;
            e.printStackTrace();
        } catch (IOException e) {
            strResultado = null;
            e.printStackTrace();
        } catch (JSONException e) {
            strResultado = null;
            e.printStackTrace();
        }
        return strResultado;
    }

    /**
     * Transforma el InputStream en un String
     * @return StringBuilder
     * */
    private StringBuilder inputStreamToString(InputStream is) throws UnsupportedEncodingException {
        String line = "";
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader rd = new BufferedReader( new InputStreamReader(is, Charset.forName("ISO-8859-1")) );
        try
        {
            while( (line = rd.readLine()) != null )
            {
                stringBuilder.append(line);
            }
        }
        catch( IOException e)
        {
            e.printStackTrace();
        }
        return stringBuilder;
    }

    /**
     * Realiza la tarea en segundo plano
     * */

    @Override
    protected String doInBackground(Void... params) {
        String resul = getRestFul();
        return resul;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null)
            delegate.resultadoOK(result);
        else
            delegate.resultadoError(result);
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(activity);
        dialog.setTitle(Constants.MESSAGE_PROCESANDO);
        dialog.setMessage(Constants.MESSAGE_WAIT);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();
        super.onPreExecute();
    }

}


