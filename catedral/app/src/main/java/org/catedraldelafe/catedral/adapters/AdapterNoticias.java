package org.catedraldelafe.catedral.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.snowdream.android.widget.SmartImageView;

import org.catedraldelafe.catedral.DetalleNoticiaActivity;
import org.catedraldelafe.catedral.R;
import org.catedraldelafe.catedral.dominio.Noticia;
import org.catedraldelafe.catedral.util.Configuration;

import java.util.ArrayList;



/**
 * Created by dduran on 15/01/2016.
 */
public class AdapterNoticias extends BaseAdapter{

    //variables
    private Activity activity;
    private ArrayList<Noticia> items;
    private String urlVideo;
    private int pos;

    public AdapterNoticias(Activity activity, ArrayList<Noticia> noticias){
        this.activity = activity;
        this.items = noticias;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //definicion de variables
        View v = convertView;
        pos = position;

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int densityDpi = metrics.densityDpi;

        // relacionamos el listview con nuestro archivo xml itemlista

        if (convertView==null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.layout, null);

            Noticia noticia = items.get(position);

            //seteo el titulo
            TextView titulo = (TextView) v.findViewById(R.id.txtTitulo);
            titulo.setText(noticia.getTitulo());

            LinearLayout ly = (LinearLayout)v.findViewById(R.id.listaImagenes);
            //LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(280, 280);
            param.setMargins(0, 0, 10, 0);
            LinearLayout.LayoutParams paramSmall = new LinearLayout.LayoutParams(140, 140);
            paramSmall.setMargins(0, 0, 10, 0);

            final Context context=v.getContext();
            String server = Configuration.getProperty("url.base", context);

            //seteo las imagenes
            for (String imagen: noticia.getImagenes()) {
                if (imagen.length() > 0){
                    SmartImageView img = new SmartImageView(context);
                    if (densityDpi <= 120){
                        img.setLayoutParams(paramSmall);
                    } else {
                        img.setLayoutParams(param);
                    }
                    img.setClickable(true);
                    img.setAdjustViewBounds(true);
                    String urlParse = server + imagen;
                    img.setTag(urlParse + "#" + position);
                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(activity, DetalleNoticiaActivity.class);
                            Bundle bundle = new Bundle();
                            pos = Integer.parseInt(v.getTag().toString().substring(v.getTag().toString().indexOf("#") + 1));
                            bundle.putString("TITULO", items.get(pos).getTitulo().toString());
                            bundle.putString("DETALLE", items.get(pos).getDescripcion().toString());
                            bundle.putString("IMAGEN", v.getTag().toString().substring(0,v.getTag().toString().indexOf("#")));
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                    });
                    try{
                        Rect rect = new Rect(img.getLeft(), img.getTop(), img.getRight(), img.getBottom());
                        img.setImageUrl(urlParse,rect);
                    } catch (Exception e){
                        e.getStackTrace();
                        img.setBackgroundResource(R.drawable.catedral1);
                    }
                    //img.setBackgroundResource(R.drawable.catedral1);
                    ly.addView(img);
                }
            }

            //seteo los videos
            for (String vid: noticia.getVideos()) {
                if (vid.length() > 0){
                    SmartImageView video = new SmartImageView(context);
                    if (densityDpi <= 120){
                        video.setLayoutParams(paramSmall);
                    } else {
                        video.setLayoutParams(param);
                    }
                    video.setClickable(true);
                    video.setAdjustViewBounds(true);
                    Rect rect = new Rect(video.getLeft(), video.getTop(), video.getRight(), video.getBottom());
                    String idV = vid.substring(vid.indexOf("*") + 1);
                    try{
                        String urlImgVideo = "http://img.youtube.com/vi/" + idV + "/0.jpg";
                        video.setImageUrl(urlImgVideo, rect);
                    }catch (Exception e){
                        e.getStackTrace();
                        video.setBackgroundResource(R.drawable.video1);
                    }
                    //video.setBackgroundResource(R.drawable.video1);

                    urlVideo = vid.substring(0, vid.indexOf("*"));
                    video.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlVideo)));
                            Log.i("Video", "Video Playing....");
                        }
                    });
                    ly.addView(video);
                }
            }
        }

        return v;
    }

}
