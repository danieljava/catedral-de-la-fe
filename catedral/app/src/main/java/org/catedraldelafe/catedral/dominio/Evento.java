package org.catedraldelafe.catedral.dominio;

/**
 * Created by mateo on 07/01/2016.
 */
public class Evento {
    private  long id;
    private String fechaHora;
    private String fecha;
    private String titulo;
    private String descripcion;
    private String estado;

    public Evento(String fechaHora, String fecha, String titulo, String descripcion, String estado) {
        this.fechaHora = fechaHora;
        this.fecha = fecha;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
