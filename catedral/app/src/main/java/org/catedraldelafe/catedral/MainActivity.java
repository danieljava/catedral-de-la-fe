package org.catedraldelafe.catedral;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.catedraldelafe.catedral.tasks.ClientBusqueda;
import org.catedraldelafe.catedral.util.Configuration;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Button btnNoticias, btnLecturaDia, btnPedidoOracion;
    private Button btnEventos, btnContacto,  btnTienda, btnSermonSemanal;
    private Button btnFacebook, btnTwitter, btnYoutube;
    private Button btnBuscar, btnCancelar;
    private Context context;
    private static final String TAG = "MainActivity";
    private Activity activity = this;
    private AlertDialog ad;
    private JSONObject jsonData;
    private final static String DATE_PATTERN = "yyyy-MM-dd'T'hh:mm:ss";
    private JSONObject valueData;//


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity.setTitle("");
        activity = this;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        context = getApplicationContext();

        // Menú de opciones
        btnNoticias = (Button) findViewById(R.id.btnNoticia);
        btnNoticias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MainActivity.this, NoticiaActivity.class);
               // startActivity(intent);
                new ClientBusqueda(activity,"",0,"Noticias").execute(Configuration.getProperty("url.base", context) + "api/posts?type=3");

            }
        });


        btnPedidoOracion = (Button) findViewById(R.id.btnPedidoOracion);
        btnPedidoOracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MainActivity.this, PedidoOracionActivity.class);
                startActivity(intent);
            }

        });

        btnContacto = (Button) findViewById(R.id.btnContacto);
        btnContacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Intent mailClient = new Intent(Intent.ACTION_VIEW);
                    mailClient.putExtra(Intent.EXTRA_EMAIL,new String[]{"info@catedraldelafe.org"});
                    mailClient.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                    startActivity(mailClient);
                }catch(Exception e){
                    Log.i(TAG,"no está disponible el cliente de correo");
                    e.printStackTrace();
                }

            }
        });

        btnEventos = (Button) findViewById(R.id.btnEvento);
        btnEventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new ClientBusqueda(activity,"",0,"Eventos").execute(Configuration.getProperty("url.base", context) + "api/events");

            }
        });

        btnLecturaDia = (Button) findViewById(R.id.btnLecturaDia);
        btnLecturaDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //final Intent i = new Intent(getApplicationContext(), OracionActivity.class);
                //startActivity(i);
                new ClientBusqueda(activity,"",0,"Lectura del Dia").execute(Configuration.getProperty("url.base", context) + "api/posts?type=1");

            }
        });

        btnSermonSemanal = (Button)findViewById(R.id.btnSermonSemanal);
        btnSermonSemanal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //final Intent i = new Intent(getApplicationContext(), SermonesActivity.class);
                //startActivity(i);
                new ClientBusqueda(activity,"",0,"Sermón Semanal").execute(Configuration.getProperty("url.base", context) + "api/posts?type=2");

            }
        });

        btnTienda = (Button) findViewById(R.id.btnTienda);
        btnTienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), TiendaActivity.class);
                startActivity(i);

            }
        });

        // Barra inferior redes sociales
        btnFacebook = (Button) findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    String url = Configuration.getProperty("url.facebook", context);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                }catch(ActivityNotFoundException anf){
                    Log.i(TAG, "ERROR al intentar abrir aplicación Facebook");
                    anf.printStackTrace();
                }


            }
        });

        btnTwitter = (Button) findViewById(R.id.btnTwitter);
        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    String url = Configuration.getProperty("url.twitter", context);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                }catch(ActivityNotFoundException anf){
                    Log.i(TAG, "ERROR al intentar abrir aplicación Twitter");
                    anf.printStackTrace();
                }

            }
        });



        btnYoutube = (Button) findViewById(R.id.btnYoutube);
        btnYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    String url = Configuration.getProperty("url.youtube", context);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                }catch (ActivityNotFoundException anf){
                    Log.i(TAG, "ERROR al intentar abrir aplicación Youtube");
                    anf.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_inicio) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidosOracion) {
            intent = new Intent(this, PedidoOracionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_lecturaDia) {
            new ClientBusqueda(activity,"",0,"Lectura del Dia").execute(Configuration.getProperty("url.base", activity) + "api/posts?type=1");

        } else if (id == R.id.nav_noticias) {
            new ClientBusqueda(activity,"",0,"Noticias").execute(Configuration.getProperty("url.base", activity) + "api/posts?type=3");

        } else if (id == R.id.nav_sermonSemanal){
            new ClientBusqueda(activity,"",0,"Sermón Semanal").execute(Configuration.getProperty("url.base", context) + "api/posts?type=2");

        }
        else if (id == R.id.nav_busqueda){

           crearDialogBusqueda(activity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Muestra el dialog para que el usuario
     * ingrese el texto a buscar
     */
    public void crearDialogBusqueda(final Activity activity){

        final AlertDialog.Builder alertDialog =  new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View v = inflater.inflate(R.layout.activity_busqueda_dialog, null);
        alertDialog.setView(v);
        alertDialog.create();

        alertDialog.setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
            Gson gson = new Gson();
            List<Object> listaData = new ArrayList<Object>();
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText palabraBuscar = (EditText) v.findViewById(R.id.etxtBusqueda);
                String palabra;
                palabra = palabraBuscar.getText().toString();
                if (!palabra.isEmpty()) {
                    int page = 1;

                    new ClientBusqueda(activity,palabra,page,"Búsqueda").execute(Configuration.getProperty("url.base", context) + Configuration.getProperty("ws.busqueda", context));


                  /* try {
                        String contenido = readJSONAssets();
                        try{
                            JSONObject jsonObject = new JSONObject(contenido);
                            // Verificar el estado del resultado de la búsqueda
                            String status = jsonObject.getString("status");

                            if( status.equals("ok") ){
                                // capturar el objeto data principal
                                JSONObject objectDataPrincipal = jsonObject.getJSONObject("data");
                                Data objetoData = new Data();
                                //Gson gson = new Gson();
                                objetoData = gson.fromJson(objectDataPrincipal.toString(),Data.class);

                                JSONArray dataItems = new JSONArray(objectDataPrincipal.getString("data"));
                                for(int i=0; i < dataItems.length();i++){
                                      valueData = dataItems.getJSONObject(i);
                                    // preguntar si es una publicacion o un evento
                                    String tipoPublicación = valueData.getString("tipo_publicacion");
                                    if(tipoPublicación.equals("publicaciones")){
                                        //es una reflexion, sermon o noticia
                                        ValueDataPublicacion valueDataPublicacion = new ValueDataPublicacion();
                                        valueDataPublicacion = gson.fromJson(valueData.toString(),ValueDataPublicacion.class);
                                       // Log.i(TAG,valueDataPublicacion.toString());
                                        listaData.add(valueDataPublicacion);

                                    }else if(tipoPublicación.equals("eventos")){
                                        // es un evento
                                        ValueDataEvento valueDataEvento = new ValueDataEvento();
                                      //  Log.i(TAG, valueData.toString());
                                        valueDataEvento = gson.fromJson(valueData.toString(), ValueDataEvento.class);
                                        listaData.add(valueDataEvento);
                                        //Toast.makeText(context,"es un evento",Toast.LENGTH_LONG).show();
                                    }

                                }
                                // la listaData copiar a la lista de objeto objetoData
                                objetoData.setData(listaData);


                                if(objetoData != null){
                                    // Lanzar activity para mostrar un listView con el resultado de la búsqueda
                                    Intent intent = new Intent(context, ResultadoBusquedaActivity.class);
                                    intent.putExtra("objetoData",objetoData);
                                    startActivity(intent);

                                }
                            }else{
                               mostrarDialogAlert("Resultado", "No se obtuvieron resultados para la búsqueda");
                            }


                        }catch(Exception ex){
                            ex.printStackTrace();
                        }



                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                } else {
                    Toast.makeText(activity, "Debe ingresar texto a buscar", Toast.LENGTH_LONG).show();
                }

            }

        });


            alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    }

            );

            alertDialog.show();


        }

    /**
     * lectura desde un archivo en carpeta assets
     * solo de prueba
     * @return
     * @throws IOException
     */
    public String readJSONAssets() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        AssetManager assetManager = getAssets();
        InputStream inputStream;
        inputStream = assetManager.open("busqueda.json");

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while((line = reader.readLine()) != null){
            stringBuilder.append(line);
        }
        inputStream.close();
        return stringBuilder.toString();

    }

    public void mostrarDialogAlert(String title, String message){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

}
