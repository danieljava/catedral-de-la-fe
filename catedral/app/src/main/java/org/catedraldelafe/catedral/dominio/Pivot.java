package org.catedraldelafe.catedral.dominio;

import java.io.Serializable;

/**
 * Created by caroapaza on 05/02/2016.
 */
public class Pivot implements Serializable {
    private int event_id;
    private int tag_id;



    public Pivot(){}

    public Pivot(int event_id, int tag_id) {
        this.event_id = event_id;
        this.tag_id = tag_id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getTag_id() {
        return tag_id;
    }

    public void setTag_id(int tag_id) {
        this.tag_id = tag_id;
    }

    @Override
    public String toString() {
        return "Pivot{" +
                "event_id=" + event_id +
                ", tag_id=" + tag_id +
                '}';
    }
}
