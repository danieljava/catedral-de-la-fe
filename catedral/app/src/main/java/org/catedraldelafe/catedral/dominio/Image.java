package org.catedraldelafe.catedral.dominio;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by caroapaza on 25/01/2016.
 */
public class Image implements Serializable{
    private int id;
    private String url;
    private int post_id;
    private String created_at;
    private String updated_at;

    public Image(){

    }

    public Image(String created_at, int id, int post_id, String updated_at, String url) {
        this.created_at = created_at;
        this.id = id;
        this.post_id = post_id;
        this.updated_at = updated_at;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Image{" +
                "created_at='" + created_at + '\'' +
                ", id=" + id +
                ", url='" + url + '\'' +
                ", post_id=" + post_id +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }
}
