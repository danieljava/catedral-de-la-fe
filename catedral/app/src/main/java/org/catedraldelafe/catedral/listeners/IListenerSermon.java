package org.catedraldelafe.catedral.listeners;

import org.catedraldelafe.catedral.dominio.Sermon;

import java.util.ArrayList;

/**
 * Created by pchacon .
 */
public interface IListenerSermon {
    public void resultSermonOk (ArrayList<Sermon> response);
    public void resultSermonError();
}